#!/bin/bash

###################################################################################################
# Script      :sqoop_eo_summary.sh
# Version     : 1.0.0
# Date        : 4th Feb 2021
# Author      : Deepak Thandra
#
# Comments    : This script gets the consolidated EO data
###################################################################################################
# Main header to specify the absolute paths
#==================================================================================================
ProcessName=$1
env_var=$2
table_filter=$3 # Default value is . for grep in manual mode
manual_run=$4
insert_env="emr${env_var}user"

start=$SECONDS
start_date=`date`
load_time=$(date +"%Y-%m-%d-%H-%M-%S" -d "$start_date")
load_timestamp=$(date +"%Y-%m-%d %H:%M:%S" -d "$start_date")
load_date=$(date +"%Y-%m-%d" -d "$start_date")
load_yymm=$(date +"%Y-%m" -d "$start_date")
DAYOFWEEK=$(date +"%u")

export HADOOP_OPTS=-Djava.security.egd=file:/dev/../dev/urandom


HDFS_ROOT_DIR=/user/"$insert_env"
HDFS_JOB_DIR="$HDFS_ROOT_DIR"/ProjectM/sqoop_eo_summary 
HDFS_MASTER_DIR="$HDFS_ROOT_DIR"/ProjectM/_master_files/sqoop_eo_summary

NFS_DIR_TEMP=/data1/hdfs/user/"$insert_env"
ROOT_DIR='/tmp'
HOME_DIR="$ROOT_DIR"/ProjectM_data/sqoop_eo_summary




LOG_DIR=/tmp/ProjectM_logs/sqoop_eo_summary/"$load_time"
LOG_FILE="$LOG_DIR"/log_for_"$load_time"
MAIL_PROCESS_NAME="sqoop_eo_summary $env_var"
###################################################################################################
# Standard process used to log messages with severity and logging into log file & email
#==================================================================================================
echolog()
{
level=
log_date=$(date --iso-8601=seconds)
html_suffix="</span><br>"
#Check for secerity i-Info, w-Warning, e-ERROR
case $1 in
   i) level="[Info]"; html_prefix="<span style='font-size:8.0pt;font-family:Consolas;color:#2E75B6'>";;
   w) level="[Warning]"; html_prefix="<span style='font-size:8.0pt;font-family:Consolas;color:#FFC000'>";;
   e) level="[ERROR]"; html_prefix="<span style='font-size:8.0pt;font-family:Consolas;color:red'>";;
   n) level=""; html_prefix="<span style='font-size:10.0pt;font-family:Consolas;color:#009900'>";;
   *) echo -e "select a valid switch\nUsage: echolog i/w/e/n 'message'";;
esac
msg=`echo "$3" | sed "s|\\\\\n|<br>|g;s|\\\\\t|\&nbsp\;\&nbsp\;\&nbsp\;\&nbsp\;|g"`
#Check for option l-log, r-report, b-both
case $2 in
   l) echo -e "-----------------------------------------------------------------------------------------------"
      echo -e "[$log_date] ${level}" "$3"
          ;;
   r) echo -e "$html_prefix${level}" "${msg}${html_suffix}" >> $EXEC_REPORT
          ;;
   b) echo -e "-----------------------------------------------------------------------------------------------"
      echo -e "[$log_date] ${level} ++****++ " "$3"
      echo -e "${html_prefix}${level}" "${msg}$html_suffix" >> $EXEC_REPORT
          ;;
   *) echo -e "select a valid switch\n Usage: echolog l/r/b 'message'"
          ;;
esac
#If Error return 1 as error for furthur use
if [ "$level" == "ERROR" ]; then
   return 1
fi
return 0
}
###################################################################################################
# Standard process to issue aws:s3: mv cp and ls commands after validating
#==================================================================================================
aws_s3_cmd()
{
counter=0
s3_cmd_err=0
while [ "$#" -ne 0 ]
do
   #capture command
   if [ $counter -eq 0 ]; then
      s3_cmd=$1
   elif [ $counter -eq 1 ]; then
      if ! [[ $1 =~ (s3:/)?/[^/]+/.* ]]; then
         echolog e b "invalid <SOURCE_BUCKET> in aws_s3_cmd:$1"
         s3_cmd_err=1
      fi
      s3_parameters="'$1'"
   #don't validate for ls command
   elif [ $counter -eq 2 ] && ( [ "$s3_cmd" == "cp" ] || [ "$s3_cmd" == "mv" ] ); then
      if ! [[ $1 =~ (s3:/)?/[^/]+/.* ]]; then
         echolog e b "invalid <TARGET_BUCKET> in aws_s3_cmd:$1"
         s3_cmd_err=1
      fi
      s3_parameters="$s3_parameters '$1'"
   #for include and exclude parameters with * quote the param
   elif [[ $1 =~ "*" ]]; then
      s3_parameters="$s3_parameters '$1'"
   else
      s3_parameters="$s3_parameters $1"
   fi
   shift
   ((counter++))
done
#using a key variable so that replace all does not find an aws[:space:]s3 here
awskey="aws"
s3key="s3"
command="$awskey $s3key $s3_cmd $s3_parameters"
#echo i l "COMMAND: $command"
#check for error
if [ $s3_cmd_err -eq 1 ]; then
   return $s3_cmd_err
else
   eval ${command}
   if [ $? -eq 1 ]; then
      return 1
   fi
fi
}
###################################################################################################
# Standard process used to capture error if any and mark load_flag
#==================================================================================================
error_check()
{
if [ "$?" -ne 0 ]; then
   if [[ -z "$1" ]]; then
      echolog e b "Error at Line: ${BASH_LINENO[*]}"
   else
      echolog e l "Error at Line: ${BASH_LINENO[*]}"
      echolog e b "ERROR! $1"
   fi
   return 1
fi
return 0
}
###################################################################################################
# Standard process used to email defined template
#==================================================================================================
MAIL_REPORT()
{
if [ "$load_flag" == "fail" ]; then
   echo 'fail' >> ${critical_flag_file}
fi
critical_flag=$(cat ${critical_flag_file} | sort -u | paste -sd:)
if ! [ -z "$critical_flag" ]; then
   load_flag='fail'
   critical_flag=":"${critical_flag}
fi
LOG_DIR_NFS=${LOG_DIR//\/tmp/${ROOT_DIR}}
#Load Status
echo "$MAIL_PROCESS_NAME,$HOSTNAME,$load_timestamp,$(( SECONDS - start )),${load_flag}${critical_flag},${LOG_DIR_NFS//\\n/|},${PARAMETERS//\\n/|}" > $LOAD_STATUS
#cat "$LOAD_STATUS" | hadoop fs -put -f - "/apps/hive/warehouse/s3_stat.db/incr_projm_load_status/${MAIL_PROCESS_NAME}_${load_time}"
#Mail
MAIL_HEADER="LOAD STATUS: $load_flag${critical_flag}\nLOAD TIME: $start_date\nLOG DIR: $LOG_DIR_NFS\nHOST NAME: $HOSTNAME\nELAPSED: $(( SECONDS - start )) seconds\n\n"
echo -e "$MAIL_HEADER"; cat $EXEC_REPORT
# This is changed from other scripts to introduce HTML in the mail body
NEW_MAIL_HEADER="<font color=\"#009900\">"${MAIL_HEADER//'\n'/<br>}"</font><br>"

#if manual run skip mail"
#if [ -z "$manual_run" ] && [ "$load_flag" != "skip" ]; then
  echolog i l "Sending email with $load_flag"

  (
  echo "From: yarn@awsemr$env_var.li.lumentuminc.net";
  echo "To: $MAIL_LIST";
  #echo "To: lakshmideepak.thandra@lumentum.com";
  echo "Subject: $MAIL_PROCESS_NAME Load - ${load_flag}${critical_flag}";
  echo "Content-Type: text/html";
  echo "MIME-Version: 1.0";
  echo "<style> table { font-family: arial, sans-serif; font-size:8.0pt; border-collapse: collapse; width: 100%; } td, th { border: 1px solid #cccccc; text-align: left; padding: 8px; } tr:nth-child(even) { background-color: #dddddd; } </style>"
  echo "<font color=#009900>";
  echo "$NEW_MAIL_HEADER";
  #echo "<table>"; cat "$OUTPUT_LOG"; echo "</table><br><br>";
  #cat "$OUTPUT_LOG"
  cat "$EXEC_REPORT";
  ) | sendmail  -t 
}
###################################################################################################
# sqoop_eo_summary
#==================================================================================================
sqoop_eo_summary()
{
      loadStart=$SECONDS

      loop_flag="success"
      sqoop_date=`date`

     max_year_cmd="select max(fiscal_year) from eo_dashboard.tbl_eo_consolidate;"
     max_year=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --silent=true --showHeader=false --outputformat=csv2 -e "$max_year_cmd"`  ;

  echolog i b "$max_year" ;
 
     max_period_cmd="select max(fiscal_period) from eo_dashboard.tbl_eo_consolidate where fiscal_year=$max_year;"
     max_per=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --silent=true --showHeader=false --outputformat=csv2 -e "$max_period_cmd"`  ;
	  
 echolog i b "$max_per" ;

     END_DATE=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --showHeader=false --outputformat=csv2 --silent=true -e "select cast( cast(end_date as date) as string ) from eo_dashboard.tbl_eo_acct_periods where period_year=$max_year and period_num=$max_per;"`
 echolog i b "Acct period ${END_DATE} " ;

     #ARCHIVE_PERIOD_DATE=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --showHeader=false --outputformat=csv2 --silent=true -e "select  max(cast(archive_period_date as string))  from data_analytics.tbl_eo_item_level_summary_archive ;"` 
	 
	 MASTER_SUMMARY_DATE=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --showHeader=false --outputformat=csv2 --silent=true -e "select cast(max(cast(loaded_date_inventory as date)) as string) from data_analytics.tbl_eo_item_level_summary; " `
 
 echolog i b "Master summary date $MASTER_SUMMARY_DATE" ;

     SUMMARY_END_DATE=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --showHeader=false --outputformat=csv2 --silent=true -e "select cast( cast( max(end_date) as date )  as string ) from eo_dashboard.tbl_eo_summary ;"` ; error_check "Retrieving Summary end date failed" || loop_flag='fail' ;

 echolog i b "Dashboard $SUMMARY_END_DATE" ;
	  
    if [[  $loop_flag == "success" ]]  ; then 
	   if [[ "$END_DATE" < "$MASTER_SUMMARY_DATE"  ]]  ; then 
	        
               if [[ "$END_DATE" != "$SUMMARY_END_DATE" ]]  ; then 	 	  
	          
	          SUMMARY_QUERY=$(cat $FINAL_DATA_DIR/sql/summary_query.sql)
	          SUMMARY_QUERY_RUN=`echo "${SUMMARY_QUERY}" | sed "s|:END_DATE|$END_DATE|"`
	  
	          beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" -e "${SUMMARY_QUERY_RUN}" ; error_check "Loading Summary data failed" || loop_flag='fail' ;
	      fi 
	
	    fi 
   fi   
	 

       LOAD_SUMMARY_DATE=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --showHeader=false --outputformat=csv2 --silent=true -e "select cast(max(cast(end_date as date)) as string) from eo_dashboard.tbl_eo_summary; " ` ; error_check "Failed Retrieving loaded summary end date" || loop_flag='fail' ;
      


	 EBS_END_DATE=`sqoop eval --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS" --query  "select to_char(max(end_date)) from xxsc.tbl_eo_summary" | grep -Eo '[0-9][0-9\-]+[0-9]'` ; error_check "Failed Retrieving EBS end date" || loop_flag='fail' ;
	  
	echolog i b "EBS Summary $EBS_END_DATE" ;
	
    if [  "$loop_flag" == "success" ]  ; then 
        
        if [[ "$LOAD_SUMMARY_DATE" != "$EBS_END_DATE"  ]]  ; then 
	 
	    echolog i b "Deleting EBS staging consolidate table" ;
           
            sqoop eval --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS" --query "delete  XXSC.tbl_eo_summary" ; error_check "Error deleting EBS Summary table" || loop_flag='fail' ;
          
           
	   
	     if [  "$loop_flag" == "success" ]  ; then    
                
	        sqoop export --connect "$EBS_URL"  --username "$EBS_USER" --password "$EBS_PASS" --table XXSC.TBL_EO_SUMMARY --hcatalog-database eo_dashboard --hcatalog-table tbl_eo_summary --input-optionally-enclosed-by "\\'" --input-escaped-by '\\'  ; error_check "Exporting Summary  data" || loop_flag='fail' ;
	      fi 
           fi
        fi
            
       EBS_COUNT=`sqoop eval --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS" --query  "select count(*) from xxsc.tbl_eo_summary where fiscal_period = ${max_per} and fiscal_year=${max_year} " | grep -Eo '[0-9]+'` ; error_check "Failed Retrieving EBS period count " || loop_flag='fail' ;         

      if [[ "$EBS_COUNT" > 0  ]] ; then 
              
              SUMMARY_UPDATE=$(cat $FINAL_DATA_DIR/sql/summary_update.sql)
              SUMMARY_INSERT=$(cat $FINAL_DATA_DIR/sql/summary_insert.sql)
	
              summary_updated=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --showHeader=false --outputformat=csv2 --silent=true -e "select  count(*)  from eo_dashboard.summary_stats where  summary_updated_date = '${EBS_END_DATE}' "` ; error_check "Failed Retrieving loaded summary date" || loop_flag='fail' 

           if [[ "$summary_updated" == 0  ]] ; then 
	
             if [ "$loop_flag" == 'success' ] ; then 
             sqoop eval --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS" --query "${SUMMARY_UPDATE}" ; error_check "Error Updating summary data " || loop_flag='fail' ;
             fi	

             if [ "$loop_flag" == 'success' ] ; then 
             sqoop eval --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS" --query "${SUMMARY_INSERT}" ; error_check "Error Inserting summary data " || loop_flag='fail' ;
             fi			  
   
        
          if [ "$loop_flag" == 'success' ] ; then 
              beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" -e "insert into table eo_dashboard.summary_stats values (current_timestamp ,'${EBS_END_DATE}' )" ; error_check "Inserting Summary Period failed" || loop_flag='fail' ;
          
           fi		
        fi
    
   fi
	

      if [ "$loop_flag" == 'fail' ]; then
        load_flag='fail' ;
      fi

}

###################################################################################################
# Main File processing happens in this function, which is invoked subsequently using tee option
#==================================================================================================
START_PROCESS()
{


   echolog i l "Removing the ec2 temp directories and files of $ProcessName"
   #if [ -d "$HOME_DIR/_stats" ]; then rm -rd "$HOME_DIR"/_stats/ ; error_check || load_flag='fail' ; fi
   if [ -d "$HOME_DIR/_data" ]; then rm -rd "$HOME_DIR"/_data/ ; error_check || load_flag='fail' ; fi
   if [ -d "$HOME_DIR/_temp" ]; then rm -rd "$HOME_DIR"/_temp/ ; error_check || load_flag='fail' ; fi

   echolog i l "Assigning directory and file variables"
   #FINAL_STAT_DIR="$HOME_DIR"/_stats ; echolog i l "FINAL_STAT_DIR: $FINAL_STAT_DIR"
   FINAL_DATA_DIR="$HOME_DIR"/_data ; echolog i l "FINAL_DATA_DIR: $FINAL_DATA_DIR"
   FINAL_TEMP_DIR="$HOME_DIR"/_temp ; echolog i l "FINAL_TEMP_DIR: $FINAL_TEMP_DIR"

   echolog i l "Creating data and temp directory"
   #mkdir -p "$FINAL_STAT_DIR" ; error_check || load_flag='fail'
   mkdir -p "$FINAL_DATA_DIR" ; error_check || load_flag='fail'
   mkdir -p "$FINAL_TEMP_DIR" ; error_check || load_flag='fail'


  # Get the master files from HDFS to local
   #----------------------------------------------------------------
   echolog i l "Getting all the process related files from hdfs to local"
   hdfs dfs -get "$HDFS_JOB_DIR"/* "$FINAL_DATA_DIR"/ ; error_check || load_flag='fail'
   hdfs dfs -get "$HDFS_MASTER_DIR"/* "$FINAL_TEMP_DIR"/ ; error_check || load_flag='fail'


   MASTER_FILE="$FINAL_TEMP_DIR"/sqoop_eo_summary_master_file.txt

   echolog i l "Logs for script $ABSOLUTE_PATH run at $start_date in node $HOSTNAME"
   echolog i l "load_date  : $load_date"
   echolog i l "load_yymm  : $load_yymm"

   LOAD_STATUS="$LOG_DIR/projm_load_status"
   echo "$MAIL_PROCESS_NAME,$HOSTNAME,$load_timestamp,0,start," > $LOAD_STATUS
   #cat "$LOAD_STATUS" | hadoop fs -put -f - "/apps/hive/warehouse/s3_stat.db/incr_projm_load_status/${MAIL_PROCESS_NAME}_${load_time}_start"

   #Variables from sourcing file
   #-----------------------------------------------------------------
   echolog i l "Getting parameters from the master file"
   [ -z "$MASTER_FILE" ] || ! [ -f "$MASTER_FILE" ] && { echolog e l "invalid MASTER_FILE $MASTER_FILE"; return; }
   source $MASTER_FILE $ProcessName

   [ -z "$HIVE_URL" ] && { echolog e l "invalid HIVE_URL $HIVE_URL"; return; } ; echo "HIVE_URL: $HIVE_URL"

   [ -z "$HIVE_USER" ] && { echolog e l "invalid HIVE_USER $HIVE_USER"; return; } ; echo "HIVE_USER: $HIVE_USER"

   [ -z "$HIVE_PASS" ] && { echolog e l "invalid HIVE_PASS $HIVE_PASS"; return; } ; echo "HIVE_PASS: $HIVE_PASS"


   [ -z "$EBS_URL" ] && { echolog e l "invalid EBS_URL $EBS_URL"; return; } ; echo "EBS_URL: $EBS_URL"

   [ -z "$EBS_USER" ] && { echolog e l "invalid EBS_USER $EBS_USER"; return; } ; echo "EBS_USER: $EBS_USER"

   [ -z "$EBS_PASS" ] && { echolog e l "invalid EBS_PASS $EBS_PASS"; return; } ; echo "EBS_PASS: $EBS_PASS"

   [ -z "$MAIL_LIST" ] && { echolog e l "invalid MAIL_LIST $MAIL_LIST"; return; } ; echo "MAIL_LIST: $MAIL_LIST"

  


  

   EXEC_REPORT="$LOG_DIR"/Execution_Report ; echo "EXEC_REPORT: $EXEC_REPORT"

   load_flag='success'
   load_flag_file="$LOG_DIR/load_flag_file"
   critical_flag_file="$LOG_DIR"/critical_flag_file ; echo "critical_flag_file: $critical_flag_file"
 
   OUTPUT_LOG="$LOG_DIR/output_file.csv"
   Sqoop_Log_Status="$LOG_DIR/sqoop_log_file.csv"
   PID_LOG="$LOG_DIR/pid_log"   

   ##########################################################################################
   fileStart=$SECONDS
   echolog i r "================================================================"
   echolog i b "Loading Summary Data "
   echolog i r "----------------------------------------------------------------"
   #-------------------------------------------------
   #Sqoop Import EBS tables
   #-------------------------------------------------
   sqoop_eo_summary
  
   cat "${LOG_DIR}/*.sqoop" | grep "\[ERROR\]"
   load_flag_fail=$(cat $load_flag_file | grep "fail" | sort -u)
   if ! [ -z "$load_flag_fail" ]; then
      load_flag=$load_flag_fail
   fi
   echolog i l "Job $load_flag in $(( SECONDS - fileStart )) seconds"
   ##########################################################################################
   LC_COLLATE=C sort -o $OUTPUT_LOG $OUTPUT_LOG
   cat $OUTPUT_LOG
   #cp $OUTPUT_LOG /data1/hdfs/apps/hive/warehouse/rma.db/sqoop_log/status_${load_time}.csv
   #sed -i "s|^|<tr><td>|g;s|$|</td></tr>|g;s|,|</td><td>|g" $OUTPUT_LOG
   #sed -i "1 s|td>|th>|g" $OUTPUT_LOG
   MAIL_REPORT
}
#####################################################################################################################
# Main process validating linux user and invokes START_PROCESS function
#==================================================================================================
#Create master dir
mkdir -p $LOG_DIR && chmod 777 -R "$LOG_DIR"
echo "Log File : $LOG_FILE"
echo "Hostname : $HOSTNAME"
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/$(basename "${BASH_SOURCE[0]}")"
USER=`whoami`
case $USER in
    yarn) echo -e "$USER is a valid user. Continuing...";;
    root) echo -e "Do not execute this script as root user. Not a good practice.\nPlease run the script as yarn user.\nUsage: sudo -u yarn $0 \nExiting..."
          
          ;;
    *) echo -e "Not valid user $USER.\nPlease run the script as yarn user.\nUsage: sudo -u yarn $0 \nExiting..."
       
       ;;
esac

if [ -z "$manual_run" ]; then
   START_PROCESS &>> $LOG_FILE
else
   START_PROCESS |& tee -a $LOG_FILE
fi

# copy the log files from /tmp to nfs mount location
LOG_PATH=ProjectM_logs/sqoop_eo_summary 
hdfs dfs  -put  /tmp/"$LOG_PATH"/*  "$HDFS_ROOT_DIR"/"$LOG_PATH"/ && rm -r /tmp/"$LOG_PATH"/
#mkdir -p "$ROOT_DIR"/"$LOG_PATH"
#cp -rn /tmp/"$LOG_PATH"/* "$ROOT_DIR"/"$LOG_PATH"/ && rm -r /tmp/"$LOG_PATH"/
#==================================================================================================