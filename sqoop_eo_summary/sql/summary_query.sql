insert overwrite table eo_dashboard.tbl_eo_summary
 select
 cast(eap.period_year as int)  fiscal_year ,
     cast(eap.period_num as int)  fiscal_period ,
     cast(eap.quarter_num as int) fiscal_qtr ,
	 cast(eap.period_name as string) period_name ,
     cast(eap.end_date as string) end_date ,
     eos.organization_code ,
     eos.inventory_item item ,
     eos.RECOMENDATION_CALC   SYS_RECOMM ,
     cast(eos.EXCESS_TO_USAGE as decimal(15,5)) NET_EXCESS_USAGE ,
     cast(eos.EXCESS_TO_DEMAND as decimal(15,5)) TOTAL_EXCESS_DEMAND ,
     cast(eos.USAGE as decimal(15,5))  USAGE_6MO ,
     cast(eos.DEMAND_12M as decimal(15,5)) GROSS_DEMAND_12MO ,
     cast(eos.DEMAND_FULL as decimal(15,5)) FULL_DEMAND,
     cast(eos.PO_QTY_OPEN  as decimal(15,5)) OPEN_PO , 
 cast(eos.supply_12m as decimal(15,5)) SUPPLY_12MO ,
 cast(eos.net_inventory_lite as decimal(15,5)) NET_INV ,
 cast(eos.item_cost as decimal(15,5)) ITEM_COST ,
  cast(eos.ohb as decimal(15,5)) NET_ONHAND 
from
    data_analytics.tbl_eo_item_level_summary eos ,
  (select  max(cast(loaded_date_inventory as date))  max_loaded_date from data_analytics.tbl_eo_item_level_summary )  ld ,
  eo_dashboard.tbl_eo_acct_periods  ean ,
  eo_dashboard.tbl_eo_acct_periods  eap
 where 
 ld.max_loaded_date between cast(ean.start_date as date) and  cast(ean.end_date as date) 
 and date_add(eap.end_date ,1) = ean.start_date