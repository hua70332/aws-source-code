MERGE INTO xxsc.xxsc_eo_dashboard_tbl a
USING xxsc.tbl_eo_summary b 
ON 
(a.fiscal_year=b.fiscal_year AND a.fiscal_period=b.fiscal_period AND a.organization_code=b.organization_code AND a.item=b.item )
WHEN MATCHED THEN
UPDATE SET 
a.SYS_RECOMM = b.SYS_RECOMM ,
a.NET_EXCESS_USAGE = b.NET_EXCESS_USAGE ,
a.TOTAL_EXCESS_DEMAND = b.TOTAL_EXCESS_DEMAND  ,
a.USAGE_6MO  = b.USAGE_6MO ,
a.GROSS_DEMAND_12MO = b.GROSS_DEMAND_12MO  ,
a.FULL_DEMAND = b.FULL_DEMAND ,
a.OPEN_PO = b.OPEN_PO ,
a.SUPPLY_12MO = b.SUPPLY_12MO ,
a.NET_INV = b.NET_INV,
a.ITEM_COST = DECODE(NVL(a.ITEM_COST,0),0,b.item_cost,a.item_cost),
a.PROVISION_REF_ID = 1,
a.last_update_date= SYSDATE