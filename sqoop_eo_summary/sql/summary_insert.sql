MERGE INTO xxsc.xxsc_eo_dashboard_tbl a
USING (SELECT * FROM xxsc.tbl_eo_summary WHERE open_po >0) b 
ON 
(a.fiscal_year=b.fiscal_year and  a.fiscal_period=b.fiscal_period  and a.organization_code=b.organization_code and a.item=b.item)
WHEN NOT MATCHED THEN 
INSERT (
a.DASHBOARD_REC_ID ,a.FISCAL_YEAR   ,a.FISCAL_QTR,a.FISCAL_PERIOD ,a.PERIOD_NAME,a.ORGANIZATION_CODE , a.ITEM , 
a.SYS_RECOMM   , a.NET_EXCESS_USAGE  , a.TOTAL_EXCESS_DEMAND , a.USAGE_6MO   , a.GROSS_DEMAND_12MO  , a.FULL_DEMAND  , a.OPEN_PO  , 
a.SUPPLY_12MO , a.NET_INV , a.ITEM_COST , a.NET_OH, a.PROVISION_REF_ID, a.creation_date  )
VALUES 
(XXSC_EO_DASHB_SEQ.NEXTVAL,b.FISCAL_YEAR, b.FISCAL_QTR,b.FISCAL_PERIOD ,b.PERIOD_NAME,b.ORGANIZATION_CODE,b.ITEM , 
b.SYS_RECOMM   , b.NET_EXCESS_USAGE  , b.TOTAL_EXCESS_DEMAND , b.USAGE_6MO   , b.GROSS_DEMAND_12MO  , b.FULL_DEMAND  , b.OPEN_PO , 
b.SUPPLY_12MO , b.NET_INV , b.ITEM_COST , b.NET_ONHAND , 1, SYSDATE )