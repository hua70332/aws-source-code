#!/bin/bash

###################################################################################################
# Script      : sqoop_eo_ebs_aws_transfer.sh
# Version     : 1.0.0
# Date        : 14th Apr 2021
# Author      : Deepak Thandra
#
# Comments    : This script gets the incr data from EBS DB
###################################################################################################
# Main header to specify the absolute paths
#==================================================================================================
ProcessName=$1
env_var=$2
table_filter=$3 # Default value is . for grep in manual mode
manual_run=$4
insert_env="emr${env_var}user"

start=$SECONDS
start_date=`date`
load_time=$(date +"%Y-%m-%d-%H-%M-%S" -d "$start_date")
load_timestamp=$(date +"%Y-%m-%d %H:%M:%S" -d "$start_date")
load_date=$(date +"%Y-%m-%d" -d "$start_date")
load_yymm=$(date +"%Y-%m" -d "$start_date")
DAYOFWEEK=$(date +"%u")

export HADOOP_OPTS=-Djava.security.egd=file:/dev/../dev/urandom

HDFS_ROOT_DIR=/user/"$insert_env"
HDFS_JOB_DIR="$HDFS_ROOT_DIR"/ProjectM/sqoop_eo_ebs_aws_transfer
HDFS_MASTER_DIR="$HDFS_ROOT_DIR"/ProjectM/_master_files/sqoop_eo_ebs_aws_transfer

NFS_DIR_TEMP=/data1/hdfs/user/"$insert_env"
ROOT_DIR='/tmp'
HOME_DIR="$ROOT_DIR"/ProjectM_data/sqoop_eo_ebs_aws_transfer


LOG_DIR=/tmp/ProjectM_logs/sqoop_eo_ebs_aws_transfer/"$load_time"
LOG_FILE="$LOG_DIR"/log_for_"$load_time"
MAIL_PROCESS_NAME="sqoop_eo_ebs_aws_transfer $env_var"
###################################################################################################
# Standard process used to log messages with severity and logging into log file & email
#==================================================================================================
echolog()
{
level=
log_date=$(date --iso-8601=seconds)
html_suffix="</span><br>"
#Check for secerity i-Info, w-Warning, e-ERROR
case $1 in
   i) level="[Info]"; html_prefix="<span style='font-size:8.0pt;font-family:Consolas;color:#2E75B6'>";;
   w) level="[Warning]"; html_prefix="<span style='font-size:8.0pt;font-family:Consolas;color:#FFC000'>";;
   e) level="[ERROR]"; html_prefix="<span style='font-size:8.0pt;font-family:Consolas;color:red'>";;
   n) level=""; html_prefix="<span style='font-size:10.0pt;font-family:Consolas;color:#009900'>";;
   *) echo -e "select a valid switch\nUsage: echolog i/w/e/n 'message'";;
esac
msg=`echo "$3" | sed "s|\\\\\n|<br>|g;s|\\\\\t|\&nbsp\;\&nbsp\;\&nbsp\;\&nbsp\;|g"`
#Check for option l-log, r-report, b-both
case $2 in
   l) echo -e "-----------------------------------------------------------------------------------------------"
      echo -e "[$log_date] ${level}" "$3"
          ;;
   r) echo -e "$html_prefix${level}" "${msg}${html_suffix}" >> $EXEC_REPORT
          ;;
   b) echo -e "-----------------------------------------------------------------------------------------------"
      echo -e "[$log_date] ${level} ++****++ " "$3"
      echo -e "${html_prefix}${level}" "${msg}$html_suffix" >> $EXEC_REPORT
          ;;
   *) echo -e "select a valid switch\n Usage: echolog l/r/b 'message'"
          ;;
esac
#If Error  as error for furthur use
if [ "$level" == "ERROR" ]; then
   return 1
fi
return 0
}
###################################################################################################
# Standard process to issue aws:s3: mv cp and ls commands after validating
#==================================================================================================
aws_s3_cmd()
{
counter=0
s3_cmd_err=0
while [ "$#" -ne 0 ]
do
   #capture command
   if [ $counter -eq 0 ]; then
      s3_cmd=$1
   elif [ $counter -eq 1 ]; then
      if ! [[ $1 =~ (s3:/)?/[^/]+/.* ]]; then
         echolog e b "invalid <SOURCE_BUCKET> in aws_s3_cmd:$1"
         s3_cmd_err=1
      fi
      s3_parameters="'$1'"
   #dont validate for ls command
   elif [ $counter -eq 2 ] && ( [ "$s3_cmd" == "cp" ] || [ "$s3_cmd" == "mv" ] ); then
      if ! [[ $1 =~ (s3:/)?/[^/]+/.* ]]; then
         echolog e b "invalid <TARGET_BUCKET> in aws_s3_cmd:$1"
         s3_cmd_err=1
      fi
      s3_parameters="$s3_parameters '$1'"
   #for include and exclude parameters with * quote the param
   elif [[ $1 =~ "*" ]]; then
      s3_parameters="$s3_parameters '$1'"
   else
      s3_parameters="$s3_parameters $1"
   fi
   shift
   ((counter++))
done
#using a key variable so that replace all does not find an aws[:space:]s3 here
awskey="aws"
s3key="s3"
command="$awskey $s3key $s3_cmd $s3_parameters"
#echo i l "COMMAND: $command"
#check for error
if [ $s3_cmd_err -eq 1 ]; then
   return $s3_cmd_err
else
   eval ${command}
   if [ $? -eq 1 ]; then
      return 1
   fi
fi
}
###################################################################################################
# Standard process used to capture error if any and mark load_flag
#==================================================================================================
error_check()
{
if [ "$?" -ne 0 ]; then
   if [[ -z "$1" ]]; then
      echolog e b "Error at Line: ${BASH_LINENO[*]}"
   else
      echolog e l "Error at Line: ${BASH_LINENO[*]}"
      echolog e b "ERROR! $1"
   fi
   return 1
fi
return 0
}
###################################################################################################
# Standard process used to email defined template
#==================================================================================================
MAIL_REPORT()
{
if [ "$load_flag" == "fail" ]; then
   echo 'fail' >> ${critical_flag_file}
fi
critical_flag=$(cat ${critical_flag_file} | sort -u | paste -sd:)
if ! [ -z "$critical_flag" ]; then
   load_flag='fail'
   critical_flag=":"${critical_flag}
fi
LOG_DIR_NFS=${LOG_DIR//\/tmp/${ROOT_DIR}}
#Load Status
echo "$MAIL_PROCESS_NAME,$HOSTNAME,$load_timestamp,$(( SECONDS - start )),${load_flag}${critical_flag},${LOG_DIR_NFS//\\n/|},${PARAMETERS//\\n/|}" > $LOAD_STATUS
#cat "$LOAD_STATUS" | hadoop fs -put -f - "/apps/hive/warehouse/s3_stat.db/incr_projm_load_status/${MAIL_PROCESS_NAME}_${load_time}"
#Mail
MAIL_HEADER="LOAD STATUS: $load_flag${critical_flag}\nLOAD TIME: $start_date\nLOG DIR: $LOG_DIR_NFS\nHOST NAME: $HOSTNAME\nELAPSED: $(( SECONDS - start )) seconds\n\n"
echo -e "$MAIL_HEADER"; cat $EXEC_REPORT
# This is changed from other scripts to introduce HTML in the mail body
NEW_MAIL_HEADER="<font color=\"#009900\">"${MAIL_HEADER//'\n'/<br>}"</font><br>"

#if manual run skip mail
if [ -z "$manual_run" ] && [ "$load_flag" != "skip" ]; then
  echolog i l "Sending email with $load_flag"
  (
  echo "From: yarn@awsemr$env_var.li.lumentuminc.net";
  echo "To: $MAIL_LIST";
  echo "Subject: $MAIL_PROCESS_NAME Load - ${load_flag}${critical_flag}";
  echo "Content-Type: text/html";
  echo "MIME-Version: 1.0";
  #echo "<style> table { font-family: arial, sans-serif; font-size:8.0pt; border-collapse: collapse; width: 100%; } td, th { border: 1px solid #cccccc; text-align: left; padding: 8px; } tr:nth-child(even) { background-color: #dddddd; } </style>"
  echo "<font color=#009900>";
  echo "$NEW_MAIL_HEADER";
  #echo "<table>"; cat "$OUTPUT_LOG"; echo "</table><br><br>";
  cat "$EXEC_REPORT";
  ) | sendmail -t
fi
}
###################################################################################################
# Sqoop  import from EBS
#==================================================================================================
sqoop_import_to_hive()
{
   loadStart=$SECONDS

   loop_flag='success'
   sqoop_date=`date`
    
    IMPORT_QUERY="select * from  xxsc_eo_dashboard_tbl "



    echolog i l "Removing stg_import_eo_consolidate  old data from tmpstg bucket"

    aws_s3_cmd rm "${TARGET_DIR/s3a/s3}" --recursive ; error_check "REMOVING $TABLE_NAME OLD DATA FROM TMPSTG BUCKET" || loop_flag='fail'


    maxupdate_cmd="select max(last_update_date) from eo_dashboard.tbl_eo_consolidate"
    maxupdate=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --silent=true --showHeader=false --outputformat=csv2 -e "$maxupdate_cmd"`  ; error_check "Retrieving max last_update_date" || loop_flag='fail'

    maxcrdate_cmd="select max(creation_date) from eo_dashboard.tbl_eo_consolidate"
    maxcreate=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --silent=true --showHeader=false --outputformat=csv2 -e "$maxcrdate_cmd"`  ; error_check "Retrieving max creation_date " || loop_flag='fail'

    filter="  where (last_update_date > to_timestamp('${maxupdate}','YYYY-MM-DD HH24:MI:SS.FF')  )  or (creation_date > to_timestamp('${maxcreate}','YYYY-MM-DD HH24:MI:SS.FF') and last_update_date is null) "

  
    echolog  i  b  "Incremental Filter $filter"

    RUN_QUERY_U=$(cat "$FINAL_DATA_DIR"/sql/update_item_data.sql)
    RUN_QUERY_UPDATE=`echo "$RUN_QUERY_U" `
    RUN_QUERY_TEMP=$(cat "$FINAL_DATA_DIR"/sql/insert_stg.sql)
    RUN_QUERY_EO=$(cat "$FINAL_DATA_DIR"/sql/insert_ebs_to_aws.sql)
  
    echolog i b "Updating item data for CM records"

  if [ "$loop_flag"='success' ] ; then 
     sqoop eval --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS" --query "${RUN_QUERY_UPDATE}" ; error_check "Error Updating item data " || loop_flag='fail'
   fi

  if [ "$loop_flag"='success' ] ; then 
     sqoop eval --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS" --query "update XXSC.xxsc_eo_dashboard_tbl set organization_code = decode(organization_code,'Fabrinet US' , 'Fabrinet Legacy' , 'HBMT' , 'Hisense') where organization_code in ( 'Fabrinet US' ,'HBMT') " ; error_check "Error Updating CM Org Code data " || loop_flag='fail'
   fi
   
    #aws s3 rm  s3://lum-projm-prod/DLF_SQOOP_TABLES/eo_dashboard/stg_import_eo_consolidate --recursive

 if [ "$loop_flag"='success' ] ; then 
    echolog i b "Importing EBS  data to AWS staging table"
    sqoop import --connect "$EBS_URL" --username "$EBS_USER"  --password "$EBS_PASS"  --query "select * from (${IMPORT_QUERY})  $filter AND  \$CONDITIONS" --target-dir "$TARGET_DIR"  --fields-terminated-by '\001' --lines-terminated-by '\n' --null-string '\\N'  --null-non-string '\\N' --hive-delims-replacement '\0D' -m 1 ; error_check "Error importing EBS data to AWS" || loop_flag='fail'
fi

  
   if [ "$loop_flag" == 'success' ] ; then 
    echolog i b "Inserting data from  AWS consolidate table into temp table"
   beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" -e  "$RUN_QUERY_TEMP" ; error_check "Error inserting data from AWS to temp table" || loop_flag='fail'
   fi 
  
   if [ "$loop_flag" == 'success' ] ; then 
   echolog i b "Overwrite non duplicate data into consolidate table" 
   beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" -e "insert overwrite  table eo_dashboard.tbl_eo_consolidate select * from eo_dashboard.stg_eo_consolidate;" ; error_check "Error overwriting non duplicate data  into consolidate table" || loop_flag='fail'
  fi

 
    if [ "$loop_flag" == 'success' ] ; then 
   echolog i b "Inserting EBS data to AWS consolidate table" 
     beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" -e  "$RUN_QUERY_EO" ; error_check "Error inserting EBS data to AWS" || loop_flag='fail'
   fi

    if [ "$loop_flag" == 'fail' ]; then
      load_flag='fail'
    fi


    echolog i b "Data load  $loop_flag in $(( SECONDS - loadStart )) seconds"
    echo "$loop_flag" >> $load_flag_file

}
###################################################################################################
# Main File processing happens in this function, which is invoked subsequently using tee option
#==================================================================================================
START_PROCESS()
{


      echolog i l "Removing the ec2 temp directories and files of $ProcessName"
   #if [ -d "$HOME_DIR/_stats" ]; then rm -rd "$HOME_DIR"/_stats/ ; error_check || load_flag='fail' ; fi
   if [ -d "$HOME_DIR/_data" ]; then rm -rd "$HOME_DIR"/_data/ ; error_check || load_flag='fail' ; fi
   if [ -d "$HOME_DIR/_temp" ]; then rm -rd "$HOME_DIR"/_temp/ ; error_check || load_flag='fail' ; fi

   echolog i l "Assigning directory and file variables"
   #FINAL_STAT_DIR="$HOME_DIR"/_stats ; echolog i l "FINAL_STAT_DIR: $FINAL_STAT_DIR"
   FINAL_DATA_DIR="$HOME_DIR"/_data ; echolog i l "FINAL_DATA_DIR: $FINAL_DATA_DIR"
   FINAL_TEMP_DIR="$HOME_DIR"/_temp ; echolog i l "FINAL_TEMP_DIR: $FINAL_TEMP_DIR"

   echolog i l "Creating data and temp directory"
   #mkdir -p "$FINAL_STAT_DIR" ; error_check || load_flag='fail'
   mkdir -p "$FINAL_DATA_DIR" ; error_check || load_flag='fail'
   mkdir -p "$FINAL_TEMP_DIR" ; error_check || load_flag='fail'


  # Get the master files from HDFS to local
   #----------------------------------------------------------------
   echolog i l "Getting all the process related files from hdfs to local"
   hdfs dfs -get "$HDFS_JOB_DIR"/* "$FINAL_DATA_DIR"/ ; error_check || load_flag='fail'
   hdfs dfs -get "$HDFS_MASTER_DIR"/* "$FINAL_TEMP_DIR"/ ; error_check || load_flag='fail'


   MASTER_FILE="$FINAL_TEMP_DIR"/sqoop_eo_ebs_aws_transfer.txt




   echolog i l "Logs for script $ABSOLUTE_PATH run at $start_date in node $HOSTNAME"
   echolog i l "load_date  : $load_date"
   echolog i l "load_yymm  : $load_yymm"

   '''
   LOAD_STATUS="$LOG_DIR/projm_load_status"
   echo "$MAIL_PROCESS_NAME,$HOSTNAME,$load_timestamp,0,start," > $LOAD_STATUS
   #cat "$LOAD_STATUS" | hadoop fs -put -f - "/apps/hive/warehouse/s3_stat.db/incr_projm_load_status/${MAIL_PROCESS_NAME}_${load_time}_start"
   '''
   #Variables from sourcing file
   #-----------------------------------------------------------------
   echolog i l "Getting parameters from the master file"
   [ -z "$MASTER_FILE" ] || ! [ -f "$MASTER_FILE" ] && { echolog e l "invalid MASTER_FILE $MASTER_FILE"; return; }
   source $MASTER_FILE $ProcessName

   [ -z "$HIVE_URL" ] && { echolog e l "invalid HIVE_URL $HIVE_URL"; return; } ; echo "HIVE_URL: $HIVE_URL"

   [ -z "$HIVE_USER" ] && { echolog e l "invalid HIVE_USER $HIVE_USER"; return; } ; echo "HIVE_USER: $HIVE_USER"

   [ -z "$HIVE_PASS" ] && { echolog e l "invalid HIVE_PASS $HIVE_PASS"; return; } ; echo "HIVE_PASS: $HIVE_PASS"

  [ -z "$EBS_URL" ] && { echolog e l "invalid EBS_URL $EBS_URL"; return; } ; echo "EBS_URL: $EBS_URL"

   [ -z "$EBS_USER" ] && { echolog e l "invalid EBS_USER $EBS_USER"; return; } ; echo "EBS_USER: $EBS_USER"

   [ -z "$EBS_PASS" ] && { echolog e l "invalid EBS_PASS $EBS_PASS"; return; } ; echo "EBS_PASS: $EBS_PASS"

    [ -z "$TARGET_DIR" ] && { echolog e l "invalid TARGET_DIR $TARGET_DIR"; return; } ; echo "TARGET_DIR: $TARGET_DIR"
 

   [ -z "$MAIL_LIST" ] && { echolog e l "invalid MAIL_LIST $MAIL_LIST"; return; } ; echo "MAIL_LIST: $MAIL_LIST"

   EXEC_REPORT="$LOG_DIR"/Execution_Report ; echo "EXEC_REPORT: $EXEC_REPORT"

   load_flag='success'
   load_flag_file="$LOG_DIR/load_flag_file"
   critical_flag_file="$LOG_DIR"/critical_flag_file ; echo "critical_flag_file: $critical_flag_file"
   #SEED_FILE=$LOG_DIR/sqoop_import_tds_seed_file.txt
   #cat $FINAL_DATA_DIR/sqoop_import_tds_seed_file.txt | grep "$table_filter" > $SEED_FILE
   OUTPUT_LOG="$LOG_DIR/output_file.csv"
   Sqoop_Log_Status="$LOG_DIR/sqoop_log_file.csv"
   PID_LOG="$LOG_DIR/pid_log"

   ##########################################################################################
   fileStart=$SECONDS
   echolog i r "================================================================"
   echolog i b "Transferring data from EBS to AWS"
   echolog i r "----------------------------------------------------------------"
   #-------------------------------------------------
   #Sqoop Import EO tables
   #-------------------------------------------------


      echolog i b "========================================================================="
      #echolog i b " $TABLE_PREFIX: Sqoop Import into $HIVE_DB.$TABLE_NAME"
      echo "Log File: $LOG_FILE_SUB"
      if [ -z "$manual_run" ]; then
         { sqoop_import_to_hive &>> $LOG_FILE_SUB ; } &
      else
         { sqoop_import_to_hive |& tee $LOG_FILE_SUB ; } &
      fi

  sqoop_import_to_hive

   if ! [ -z "$load_flag_fail" ]; then
      load_flag=$load_flag_fail
   fi
   echolog i l "EBS to AWS  data transfer  $load_flag in $(( SECONDS - fileStart )) seconds"
   ##########################################################################################
   LC_COLLATE=C sort -o $OUTPUT_LOG $OUTPUT_LOG
   cat $OUTPUT_LOG
   #cp $OUTPUT_LOG /data1/hdfs/apps/hive/warehouse/rma.db/sqoop_log/status_${load_time}.csv

   MAIL_REPORT
}
#####################################################################################################################
# Main process validating linux user and invokes START_PROCESS function
#==================================================================================================
#Create master dir
mkdir -p $LOG_DIR && chmod 777 -R "$LOG_DIR"
echo "Log File : $LOG_FILE"
echo "Hostname : $HOSTNAME"
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/$(basename "${BASH_SOURCE[0]}")"
USER=`whoami`
case $USER in
    yarn) echo -e "$USER is a valid user. Continuing...";;
    root) echo -e "Do not execute this script as root user. Not a good practice.\nPlease run the script as yarn user.\nUsage: sudo -u yarn $0 \nExiting..."
          
          ;;
    *) echo -e "Not valid user $USER.\nPlease run the script as yarn user.\nUsage: sudo -u yarn $0 \nExiting..."
       
       ;;
esac

if [ -z "$manual_run" ]; then
   START_PROCESS &>> $LOG_FILE
else
   START_PROCESS |& tee -a $LOG_FILE
fi

# copy the log files from /tmp to nfs mount location
LOG_PATH=ProjectM_logs/sqoop_eo_ebs_aws_transfer
hdfs dfs  -put  /tmp/"$LOG_PATH"/*  "$HDFS_ROOT_DIR"/"$LOG_PATH"/ && rm -r /tmp/"$LOG_PATH"/
#mkdir -p "$ROOT_DIR"/"$LOG_PATH"
#cp -rn /tmp/"$LOG_PATH"/* "$ROOT_DIR"/"$LOG_PATH"/ && rm -r /tmp/"$LOG_PATH"/
#==================================================================================================