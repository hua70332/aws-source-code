update xxsc.xxsc_eo_dashboard_tbl  eo set
(item_type ,gl_line_code,gl_line_descr,item_id,item_descr,item_status,buyer_name,prod_heirarchy,L4_BUS_UNIT)
= (
select
item.user_item_type ,item.GL_PRODUCT_LINE,item.GL_PRODUCT_LINE_DESC,
substr(item.item_org_identifier,1,instr(item.item_org_identifier,'~')-1 ),
item.description,item.inventory_item_status_code,item.default_buyer,
 item.product_hierarchy,item.L4_BUSINESS_UNIT_DESC from 
 INV.MTL_PARAMETERS param ,apps.XXSC_INV_ITEM_MST_ATTRIBUTES item 

where
eo.item_type is null
and decode(upper(eo.organization_code) ,'FABRINET US','393','FABRINET LEGACY','393','FABRINET UK','253',
'HISENSE','392','HBMT','392','VENTURE','254','ELASER','395',eo.organization_code) = param.organization_code
and item.item=(case when substr(eo.item,1,4)='OROP' then substr(eo.item,5)
                         when substr(eo.item,1,3)='ORV' then substr(eo.item,4)
                          when substr(eo.item,1,3)='ORY' then substr(eo.item,4)
                           when substr(eo.item,1,2)='OR' then substr(eo.item,3)
                           else eo.item end )
and substr(item.item_org_identifier,instr(item.item_org_identifier,'~')+1 )=param.organization_id
)
where exists
(select 1 from 
INV.MTL_PARAMETERS param ,apps.XXSC_INV_ITEM_MST_ATTRIBUTES item 
where
substr(item.item_org_identifier,instr(item.item_org_identifier,'~')+1 )=param.organization_id
and
eo.item_type is null
and decode(upper(eo.organization_code) ,'FABRINET US','393','FABRINET LEGACY','393','FABRINET UK','253',
'HISENSE','392','HBMT','392','VENTURE','254','ELASER','395',eo.organization_code) = param.organization_code
and item.item=(case when substr(eo.item,1,4)='OROP' then substr(eo.item,5)
                         when substr(eo.item,1,3)='ORV' then substr(eo.item,4)
                          when substr(eo.item,1,3)='ORY' then substr(eo.item,4)
                           when substr(eo.item,1,2)='OR' then substr(eo.item,3)
                           else eo.item end ) )