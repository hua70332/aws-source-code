#!/bin/bash

###################################################################################################
# Script      :sqoop_import_eo_consolidate.sh
# Version     : 1.0.0
# Date        : 14th Apr 2021
# Author      : Deepak Thandra
#
# Comments    : This script gets the consolidated EO data
###################################################################################################
# Main header to specify the absolute paths
#==================================================================================================
ProcessName=$1
env_var=$2
table_filter=$3 # Default value is . for grep in manual mode
manual_run=$4
insert_env="emr${env_var}user"

start=$SECONDS
start_date=`date`
load_time=$(date +"%Y-%m-%d-%H-%M-%S" -d "$start_date")
load_timestamp=$(date +"%Y-%m-%d %H:%M:%S" -d "$start_date")
load_date=$(date +"%Y-%m-%d" -d "$start_date")
load_yymm=$(date +"%Y-%m" -d "$start_date")
DAYOFWEEK=$(date +"%u")

export HADOOP_OPTS=-Djava.security.egd=file:/dev/../dev/urandom


HDFS_ROOT_DIR=/user/"$insert_env"
HDFS_JOB_DIR="$HDFS_ROOT_DIR"/ProjectM/sqoop_import_eo_consolidate 
HDFS_MASTER_DIR="$HDFS_ROOT_DIR"/ProjectM/_master_files/sqoop_import_eo_consolidate

NFS_DIR_TEMP=/data1/hdfs/user/"$insert_env"
ROOT_DIR='/tmp'
HOME_DIR="$ROOT_DIR"/ProjectM_data/sqoop_import_eo_consolidate
LOG_DIR=/tmp/ProjectM_logs/sqoop_import_eo_consolidate/"$load_time"
LOG_FILE="$LOG_DIR"/log_for_"$load_time"
MAIL_PROCESS_NAME="sqoop_import_eo_consolidate $env_var"
###################################################################################################
# Standard process used to log messages with severity and logging into log file & email
#==================================================================================================
echolog()
{
level=
log_date=$(date --iso-8601=seconds)
html_suffix="</span><br>"
#Check for secerity i-Info, w-Warning, e-ERROR
case $1 in
   i) level="[Info]"; html_prefix="<span style='font-size:8.0pt;font-family:Consolas;color:#2E75B6'>";;
   w) level="[Warning]"; html_prefix="<span style='font-size:8.0pt;font-family:Consolas;color:#FFC000'>";;
   e) level="[ERROR]"; html_prefix="<span style='font-size:8.0pt;font-family:Consolas;color:red'>";;
   n) level=""; html_prefix="<span style='font-size:10.0pt;font-family:Consolas;color:#009900'>";;
   *) echo -e "select a valid switch\nUsage: echolog i/w/e/n 'message'";;
esac
msg=`echo "$3" | sed "s|\\\\\n|<br>|g;s|\\\\\t|\&nbsp\;\&nbsp\;\&nbsp\;\&nbsp\;|g"`
#Check for option l-log, r-report, b-both
case $2 in
   l) echo -e "-----------------------------------------------------------------------------------------------"
      echo -e "[$log_date] ${level}" "$3"
          ;;
   r) echo -e "$html_prefix${level}" "${msg}${html_suffix}" >> $EXEC_REPORT
          ;;
   b) echo -e "-----------------------------------------------------------------------------------------------"
      echo -e "[$log_date] ${level} ++****++ " "$3"
      echo -e "${html_prefix}${level}" "${msg}$html_suffix" >> $EXEC_REPORT
          ;;
   *) echo -e "select a valid switch\n Usage: echolog l/r/b 'message'"
          ;;
esac
#If Error return 1 as error for furthur use
if [ "$level" == "ERROR" ]; then
   return 1
fi
return 0
}
###################################################################################################
# Standard process to issue aws:s3: mv cp and ls commands after validating
#==================================================================================================
aws_s3_cmd()
{
counter=0
s3_cmd_err=0
while [ "$#" -ne 0 ]
do
   #capture command
   if [ $counter -eq 0 ]; then
      s3_cmd=$1
   elif [ $counter -eq 1 ]; then
      if ! [[ $1 =~ (s3:/)?/[^/]+/.* ]]; then
         echolog e b "invalid <SOURCE_BUCKET> in aws_s3_cmd:$1"
         s3_cmd_err=1
      fi
      s3_parameters="'$1'"
   #don't validate for ls command
   elif [ $counter -eq 2 ] && ( [ "$s3_cmd" == "cp" ] || [ "$s3_cmd" == "mv" ] ); then
      if ! [[ $1 =~ (s3:/)?/[^/]+/.* ]]; then
         echolog e b "invalid <TARGET_BUCKET> in aws_s3_cmd:$1"
         s3_cmd_err=1
      fi
      s3_parameters="$s3_parameters '$1'"
   #for include and exclude parameters with * quote the param
   elif [[ $1 =~ "*" ]]; then
      s3_parameters="$s3_parameters '$1'"
   else
      s3_parameters="$s3_parameters $1"
   fi
   shift
   ((counter++))
done
#using a key variable so that replace all does not find an aws[:space:]s3 here
awskey="aws"
s3key="s3"
command="$awskey $s3key $s3_cmd $s3_parameters"
#echo i l "COMMAND: $command"
#check for error
if [ $s3_cmd_err -eq 1 ]; then
   return $s3_cmd_err
else
   eval ${command}
   if [ $? -eq 1 ]; then
      return 1
   fi
fi
}
###################################################################################################
# Standard process used to capture error if any and mark load_flag
#==================================================================================================
error_check()
{
if [ "$?" -ne 0 ]; then
   if [[ -z "$1" ]]; then
      echolog e b "Error at Line: ${BASH_LINENO[*]}"
   else
      echolog e l "Error at Line: ${BASH_LINENO[*]}"
      echolog e b "ERROR! $1"
   fi
   return 1
fi
return 0
}
###################################################################################################
# Standard process used to email defined template
#==================================================================================================
MAIL_REPORT()
{
if [ "$load_flag" == "fail" ]; then
   echo 'fail' >> ${critical_flag_file}
fi
critical_flag=$(cat ${critical_flag_file} | sort -u | paste -sd:)
if ! [ -z "$critical_flag" ]; then
   load_flag='fail'
   critical_flag=":"${critical_flag}
fi
LOG_DIR_NFS=${LOG_DIR//\/tmp/${ROOT_DIR}}
#Load Status
echo "$MAIL_PROCESS_NAME,$HOSTNAME,$load_timestamp,$(( SECONDS - start )),${load_flag}${critical_flag},${LOG_DIR_NFS//\\n/|},${PARAMETERS//\\n/|}" > $LOAD_STATUS
#cat "$LOAD_STATUS" | hadoop fs -put -f - "/apps/hive/warehouse/s3_stat.db/incr_projm_load_status/${MAIL_PROCESS_NAME}_${load_time}"
#Mail
MAIL_HEADER="LOAD STATUS: $load_flag${critical_flag}\nLOAD TIME: $start_date\nLOG DIR: $LOG_DIR_NFS\nHOST NAME: $HOSTNAME\nELAPSED: $(( SECONDS - start )) seconds\n\n"
echo -e "$MAIL_HEADER"; cat $EXEC_REPORT
# This is changed from other scripts to introduce HTML in the mail body
NEW_MAIL_HEADER="<font color=\"#009900\">"${MAIL_HEADER//'\n'/<br>}"</font><br>"

#if manual run skip mail"
#if [ -z "$manual_run" ] && [ "$load_flag" != "skip" ]; then
  echolog i l "Sending email with $load_flag"

  (
  echo "From: yarn@awsemr$env_var.li.lumentuminc.net";
  echo "To: $MAIL_LIST";
   echo "Subject: $MAIL_PROCESS_NAME Load - ${load_flag}${critical_flag}";
  echo "Content-Type: text/html";
  echo "MIME-Version: 1.0";
  echo "<style> table { font-family: arial, sans-serif; font-size:8.0pt; border-collapse: collapse; width: 100%; } td, th { border: 1px solid #cccccc; text-align: left; padding: 8px; } tr:nth-child(even) { background-color: #dddddd; } </style>"
  echo "<font color=#009900>";
  echo "$NEW_MAIL_HEADER";
  #echo "<table>"; cat "$OUTPUT_LOG"; echo "</table><br><br>";
  #cat "$OUTPUT_LOG"
  cat "$EXEC_REPORT";
  ) | sendmail  -t 
}
###################################################################################################
# sqoop_import_eo_consolidate
#==================================================================================================
sqoop_import_to_hive()
{
      loadStart=$SECONDS

      loop_flag='success'
      sqoop_date=`date`

      
      IMPORT_QUERY_ACCT=$(cat "$FINAL_DATA_DIR"/sql/tbl_eo_acct_periods.sql)
      
      IMPORT_QUERY_TRANS=$(cat "$FINAL_DATA_DIR"/sql/stg_eo_usage.sql)
      


    if [ "$loop_flag" == 'success' ] ; then


      max_year_cmd="select max(fiscal_year) from eo_dashboard.tbl_eo_consolidate;"
      max_year=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --silent=true --showHeader=false --outputformat=csv2 -e "$max_year_cmd"`  ;
      max_qtr_cmd="select max(fiscal_qtr) from eo_dashboard.tbl_eo_consolidate where fiscal_year=$max_year;"
      max_qtr=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --silent=true --showHeader=false --outputformat=csv2 -e "$max_qtr_cmd"`  ;

      max_period_cmd="select max(fiscal_period) from eo_dashboard.tbl_eo_consolidate where fiscal_year=$max_year;"
      max_per=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --silent=true --showHeader=false --outputformat=csv2 -e "$max_period_cmd"`  ;

      if [ "$max_per" == 12 ] ; then
	    cur_year=$(($max_year+1))
	    cur_per=1
      else
        cur_year=$max_year
        cur_per=$(($max_per+1))
      fi

      PER_YEAR=$cur_year
      PER_NUM=$cur_per
      INV_PERIOD=$cur_year'-'$cur_per


       echolog i b "$cur_year - $cur_per"

      PER_NAME=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --showHeader=false --outputformat=csv2 --silent=true -e "select period_name from eo_dashboard.tbl_eo_acct_periods where period_year=$cur_year and period_num=$cur_per;"`

      END_TIME=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --showHeader=false --outputformat=csv2 --silent=true -e "select      substr(date_add(end_date,1) ,1,10) from eo_dashboard.tbl_eo_acct_periods where period_year=$cur_year and period_num=$cur_per;"`

      CUR_TIME=`beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" --showHeader=false --outputformat=csv2 --silent=true -e "select substr(current_timestamp,1,10) from eo_dashboard.tbl_eo_acct_periods where period_year=$cur_year and period_num=$cur_per;"`



      CUR_DATE=$(date -d "${CUR_TIME}" "+%s")
      END_DATE=$(date -d "${END_TIME}" "+%s")

      echolog i b "$END_DATE -- $CUR_DATE"

      if [  "$END_DATE" -le "$CUR_DATE" ]   ; then

          filter="  where fiscal_period=${cur_per} and fiscal_year=${cur_year} "
      
      
          echolog i b "filter is $filter"

          aws_s3_cmd rm "${TARGET_DIR_TRANS/s3a/s3}" --recursive ; error_check "REMOVING  OLD DATA FROM TMP TRANSACTIONS  BUCKET" || loop_flag='fail'
          
          echolog i b "Loading  Data into staging transactions table"

          if [ "$loop_flag"='success' ] ; then 

              sqoop import --connect "$EBS_URL"  --username "$EBS_USER"  --password "$EBS_PASS" --query "select * from (${IMPORT_QUERY_TRANS} ${filter} )  where \$CONDITIONS" --target-dir "$TARGET_DIR_TRANS"  --fields-terminated-by '\001' --lines-terminated-by '\n' --null-string '\\N'  --null-non-string '\\N' --hive-delims-replacement '\0D' -m 1  &>>  $LOG_DIR/stg_eo_usage.sqoop  ;   error_check "Sqooping failed for transactions periods " || loop_flag='fail' ;
              hive_records_trans=$(cat $LOG_DIR/stg_eo_usage.sqoop  | grep "Retrieved"  | sed -n "s|.*Retrieved \([0-9]*\).*|\1|p" | paste -sd: ) 

                echolog i b "Imported $hive_records_trans into  stg_eo_usage table"

            fi

                
      echolog i b  "Records loading into  accounting periods table"
      aws s3 rm "${TARGET_DIR_ACCT/s3a/s3}" --recursive ; error_check "Removing old data in  accounting periods table " ;
      sqoop import --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS"   --query "select * from (${IMPORT_QUERY_ACCT}) WHERE \$CONDITIONS" --target-dir "$TARGET_DIR_ACCT"  --fields-terminated-by '\001' --lines-terminated-by '\n' --null-string '\\N'  --null-non-string '\\N' --hive-delims-replacement '\0D' -m 1 &>> $LOG_DIR/tbl_eo_acct_periods.sqoop  ;   error_check "Sqooping failed for accounting periods " || loop_flag='fail' ;
     hive_records=$(cat $LOG_DIR/tbl_eo_acct_periods.sqoop  | grep "Retrieved"  | sed -n "s|.*Retrieved \([0-9]*\).*|\1|p" | paste -sd: ) 

     echolog i b "Imported $hive_records into  tbl_eo_acct_periods table"


           

        if [ "$loop_flag" == 'success' ] ; then 
            
            RUN_QUERY_U=$(cat $FINAL_DATA_DIR/sql/per_eo_usage.sql)
	    RUN_QUERY_USAGE=`echo "$RUN_QUERY_U" | sed "s|:PER_NUM|$PER_NUM|" | sed "s|:PER_YEAR|$PER_YEAR|"`
 
          
	  beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" -e "${RUN_QUERY_USAGE}" ; error_check "Loading Current Period Transactions failed" || loop_flag='fail' ;
        fi 

      if [ "$loop_flag" == 'success' ] ; then 
          
         IMPORT_QUERY_ONHAND=$(cat "$FINAL_DATA_DIR"/sql/stg_eo_onhand.sql)
                
          RUN_QUERY_ON=$(cat $FINAL_DATA_DIR/sql/stg_eo_onhand.sql)
	  RUN_QUERY_ONHAND=`echo "$RUN_QUERY_ON" | sed "s|:PER_NUM|$PER_NUM|" | sed "s|:PER_YEAR|$PER_YEAR|"`
          echolog i b  "Records loading into  stg_eo_onhand table"
           aws s3 rm "${TARGET_DIR_ONHAND/s3a/s3}" --recursive ; error_check "Removing old data in onhand table " ;
          echolog i b  "Records loading into  onhand table"
          sqoop import --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS"   --query "select * from (${RUN_QUERY_ONHAND}  ) WHERE \$CONDITIONS" --target-dir "$TARGET_DIR_ONHAND"  --fields-terminated-by '\001' --lines-terminated-by '\n' --null-string '\\N'  --null-non-string '\\N' --hive-delims-replacement '\0D' -m 1 &>> $LOG_DIR/stg_eo_onhand.sqoop  ;   error_check "Sqooping failed for onhand table " || loop_flag='fail' ;
          hive_records=$(cat $LOG_DIR/stg_eo_onhand.sqoop  | grep "Retrieved"  | sed -n "s|.*Retrieved \([0-9]*\).*|\1|p" | paste -sd: ) 

          echolog i b "Imported $hive_records into  stg_eo_onhand table"

        fi 


	   RUN_QUERY_P=$(cat $FINAL_DATA_DIR/sql/eo_consolidate_data.sql)
	   RUN_QUERY_PERIOD=`echo "$RUN_QUERY_P" | sed "s|:PER_NUM|$PER_NUM|" | sed "s|:PER_YEAR|$PER_YEAR|"`
          
          if [ "$loop_flag" == 'success' ] ; then
	   beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" -e "$RUN_QUERY_PERIOD" ; error_check "Period data failed " || loop_flag='fail'
          fi

         if [ "$loop_flag" == 'success' ] ; then 
       
            echolog i b "Deleting EBS staging consolidate table"
           
           sqoop eval --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS" --query "delete  xxsc_eo_consolidate_stg" ; error_check "Error deleting EBS staging consolidate table" || loop_flag='fail' ;
          fi
            
         columns_list=$(cat "$FINAL_DATA_DIR"/sql/column_list.txt)

          if [ "$loop_flag" == 'success' ] ; then
	   echolog i b "Exporting data  to EBS staging consolidate table"
          eval  sqoop export --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS" --table XXSC_EO_CONSOLIDATE_STG --hcatalog-database eo_dashboard --hcatalog-table stg_export_eo_consolidate --input-optionally-enclosed-by "\\'" --input-escaped-by '\\'  --columns \"$columns_list\" ; error_check "Error exporting staging consolidate table " || loop_flag='fail'
          fi 

          if [ "$loop_flag" == 'success' ] ; then
           echolog i b "Updating and Inserting  data in EBS  dashboard table" 
           RUN_QUERY_UPDATE=$(cat "$FINAL_DATA_DIR"/sql/merge_item_data.sql)
            sqoop eval  --connect "$EBS_URL" --username "$EBS_USER" --password "$EBS_PASS" --query "${RUN_QUERY_UPDATE}" ; error_check "Error Updating item data" || loop_flag='fail'
          fi

          if [ "$loop_flag" == 'success' ] ; then
              echolog i b " Reloading ORC Table tbl_eo_usage  data from incr table"
              beeline_command="insert into  table eo_dashboard.tbl_eo_usage  select  a.* from eo_dashboard.stg_eo_usage a;"
              beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" -e "$beeline_command" ; error_check "RELOADING tbl_eo_usage TABLE" || loop_flag='fail'
          fi

              
         if [ "$loop_flag" == 'success' ] ; then
           echolog i b " Inserting  data in history onhand table" 
	   ins_hist_onhand="insert into eo_dashboard.tbl_eo_onhand select * from eo_dashboard.stg_eo_onhand"
           beeline -u "$HIVE_URL" -n "$HIVE_USER" -p "$HIVE_PASS" -e "$ins_hist_onhand" ;  error_check "Error Inserting   data into History onhand table " || loop_flag='fail'
        fi


    fi
    fi 

      if [ "$loop_flag" == 'fail' ]; then
        load_flag='fail'
      fi

}

###################################################################################################
# Main File processing happens in this function, which is invoked subsequently using tee option
#==================================================================================================
START_PROCESS()
{


   echolog i l "Removing the ec2 temp directories and files of $ProcessName"
   #if [ -d "$HOME_DIR/_stats" ]; then rm -rd "$HOME_DIR"/_stats/ ; error_check || load_flag='fail' ; fi
   if [ -d "$HOME_DIR/_data" ]; then rm -rd "$HOME_DIR"/_data/ ; error_check || load_flag='fail' ; fi
   if [ -d "$HOME_DIR/_temp" ]; then rm -rd "$HOME_DIR"/_temp/ ; error_check || load_flag='fail' ; fi

   echolog i l "Assigning directory and file variables"
   #FINAL_STAT_DIR="$HOME_DIR"/_stats ; echolog i l "FINAL_STAT_DIR: $FINAL_STAT_DIR"
   FINAL_DATA_DIR="$HOME_DIR"/_data ; echolog i l "FINAL_DATA_DIR: $FINAL_DATA_DIR"
   FINAL_TEMP_DIR="$HOME_DIR"/_temp ; echolog i l "FINAL_TEMP_DIR: $FINAL_TEMP_DIR"

   echolog i l "Creating data and temp directory"
   #mkdir -p "$FINAL_STAT_DIR" ; error_check || load_flag='fail'
   mkdir -p "$FINAL_DATA_DIR" ; error_check || load_flag='fail'
   mkdir -p "$FINAL_TEMP_DIR" ; error_check || load_flag='fail'


  # Get the master files from HDFS to local
   #----------------------------------------------------------------
   echolog i l "Getting all the process related files from hdfs to local"
   hdfs dfs -get "$HDFS_JOB_DIR"/* "$FINAL_DATA_DIR"/ ; error_check || load_flag='fail'
   hdfs dfs -get "$HDFS_MASTER_DIR"/* "$FINAL_TEMP_DIR"/ ; error_check || load_flag='fail'


   MASTER_FILE="$FINAL_TEMP_DIR"/sqoop_import_eo_consolidate_master_file.txt

   echolog i l "Logs for script $ABSOLUTE_PATH run at $start_date in node $HOSTNAME"
   echolog i l "load_date  : $load_date"
   echolog i l "load_yymm  : $load_yymm"

   LOAD_STATUS="$LOG_DIR/projm_load_status"
   echo "$MAIL_PROCESS_NAME,$HOSTNAME,$load_timestamp,0,start," > $LOAD_STATUS
   #cat "$LOAD_STATUS" | hadoop fs -put -f - "/apps/hive/warehouse/s3_stat.db/incr_projm_load_status/${MAIL_PROCESS_NAME}_${load_time}_start"

   #Variables from sourcing file
   #-----------------------------------------------------------------
   echolog i l "Getting parameters from the master file"
   [ -z "$MASTER_FILE" ] || ! [ -f "$MASTER_FILE" ] && { echolog e l "invalid MASTER_FILE $MASTER_FILE"; return; }
   source $MASTER_FILE $ProcessName

   [ -z "$HIVE_URL" ] && { echolog e l "invalid HIVE_URL $HIVE_URL"; return; } ; echo "HIVE_URL: $HIVE_URL"

   [ -z "$HIVE_USER" ] && { echolog e l "invalid HIVE_USER $HIVE_USER"; return; } ; echo "HIVE_USER: $HIVE_USER"

   [ -z "$HIVE_PASS" ] && { echolog e l "invalid HIVE_PASS $HIVE_PASS"; return; } ; echo "HIVE_PASS: $HIVE_PASS"


   [ -z "$EBS_URL" ] && { echolog e l "invalid EBS_URL $EBS_URL"; return; } ; echo "EBS_URL: $EBS_URL"

   [ -z "$EBS_USER" ] && { echolog e l "invalid EBS_USER $EBS_USER"; return; } ; echo "EBS_USER: $EBS_USER"

   [ -z "$EBS_PASS" ] && { echolog e l "invalid EBS_PASS $EBS_PASS"; return; } ; echo "EBS_PASS: $EBS_PASS"

   [ -z "$MAIL_LIST" ] && { echolog e l "invalid MAIL_LIST $MAIL_LIST"; return; } ; echo "MAIL_LIST: $MAIL_LIST"

  
   [ -z "$TARGET_DIR_ACCT" ] && { echolog e l "invalid TARGET_DIR_ACCT $TARGET_DIR_ACCT"; return; } ; echo "TARGET_DIR_ACCT: $TARGET_DIR_ACCT"

   [ -z "$TARGET_DIR_TRANS" ] && { echolog e l "invalid TARGET_DIR_TRANS $TARGET_DIR_TRANS"; return; } ; echo "TARGET_DIR_TRANS: $TARGET_DIR_TRANS"

   [ -z "$TARGET_DIR_IPR_ISO" ] && { echolog e l "invalid TARGET_DIR_IPR_ISO $TARGET_DIR_IPR_ISO"; return; } ; echo "TARGET_DIR_IPR_ISO: $TARGET_DIR_IPR_ISO"

    [ -z "$TARGET_DIR_ONHAND" ] && { echolog e l "invalid TARGET_DIR_ONHAND $TARGET_DIR_ONHAND"; return; } ; echo "TARGET_DIR_ONHAND: $TARGET_DIR_ONHAND"


   EXEC_REPORT="$LOG_DIR"/Execution_Report ; echo "EXEC_REPORT: $EXEC_REPORT"

   load_flag='success'
   load_flag_file="$LOG_DIR/load_flag_file"
   critical_flag_file="$LOG_DIR"/critical_flag_file ; echo "critical_flag_file: $critical_flag_file"
   SEED_FILE=$LOG_DIR/sqoop_import_eo_consolidate_seed_file.txt
   cat $FINAL_DATA_DIR/sqoop_import_eo_consolidate_seed_file.txt | grep "$table_filter" > $SEED_FILE
   OUTPUT_LOG="$LOG_DIR/output_file.csv"
   Sqoop_Log_Status="$LOG_DIR/sqoop_log_file.csv"
   PID_LOG="$LOG_DIR/pid_log"   

   ##########################################################################################
   fileStart=$SECONDS
   echolog i r "================================================================"
   echolog i b "Loading Data from EBS DB to HIVE DB "
   echolog i r "----------------------------------------------------------------"
   #-------------------------------------------------
   #Sqoop Import EBS tables
   #-------------------------------------------------
   sqoop_import_to_hive
  
   cat "${LOG_DIR}/*.sqoop" | grep "\[ERROR\]"
   load_flag_fail=$(cat $load_flag_file | grep "fail" | sort -u)
   if ! [ -z "$load_flag_fail" ]; then
      load_flag=$load_flag_fail
   fi
   echolog i l "EBS to HIVE Load $load_flag in $(( SECONDS - fileStart )) seconds"
   ##########################################################################################
   LC_COLLATE=C sort -o $OUTPUT_LOG $OUTPUT_LOG
   cat $OUTPUT_LOG
   #cp $OUTPUT_LOG /data1/hdfs/apps/hive/warehouse/rma.db/sqoop_log/status_${load_time}.csv
   #sed -i "s|^|<tr><td>|g;s|$|</td></tr>|g;s|,|</td><td>|g" $OUTPUT_LOG
   #sed -i "1 s|td>|th>|g" $OUTPUT_LOG
   MAIL_REPORT
}
#####################################################################################################################
# Main process validating linux user and invokes START_PROCESS function
#==================================================================================================
#Create master dir
mkdir -p $LOG_DIR && chmod 777 -R "$LOG_DIR"
echo "Log File : $LOG_FILE"
echo "Hostname : $HOSTNAME"
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/$(basename "${BASH_SOURCE[0]}")"
USER=`whoami`
case $USER in
    yarn) echo -e "$USER is a valid user. Continuing...";;
    root) echo -e "Do not execute this script as root user. Not a good practice.\nPlease run the script as yarn user.\nUsage: sudo -u yarn $0 \nExiting..."
          
          ;;
    *) echo -e "Not valid user $USER.\nPlease run the script as yarn user.\nUsage: sudo -u yarn $0 \nExiting..."
       
       ;;
esac

if [ -z "$manual_run" ]; then
   START_PROCESS &>> $LOG_FILE
else
   START_PROCESS |& tee -a $LOG_FILE
fi

# copy the log files from /tmp to nfs mount location
LOG_PATH=ProjectM_logs/sqoop_import_eo_consolidate
hdfs dfs  -put  /tmp/"$LOG_PATH"/*  "$HDFS_ROOT_DIR"/"$LOG_PATH"/ && rm -r /tmp/"$LOG_PATH"/
#mkdir -p "$ROOT_DIR"/"$LOG_PATH"
#cp -rn /tmp/"$LOG_PATH"/* "$ROOT_DIR"/"$LOG_PATH"/ && rm -r /tmp/"$LOG_PATH"/
#==================================================================================================