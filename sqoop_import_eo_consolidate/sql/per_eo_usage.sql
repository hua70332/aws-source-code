insert overwrite table eo_dashboard.per_eo_usage 
select 
 fiscal_year,
 fiscal_period,
 organization_code,
 organization_id,
 inventory_item_id,
 inventory_item,
sum(case when std_order_ship is null then 0 else std_order_ship end )  std_ord_ship,
  sum( case when con_order_ship is null then 0 else con_order_ship end ) con_ord_ship,
  sum( case when wip_issue is null then 0 else wip_issue end ) wip_issue,
    sum( case when wip_completion is null then 0 else wip_completion end ) wip_completion,
  sum( case when int_ship is null then 0 else int_ship end ) iso_ship,
  sum( case when int_rcv is null then 0 else int_rcv end ) int_order_rcv,
   sum( case when misc_issue is null then 0 else misc_issue end )   misc_issue ,
   sum( case when eo_scrap is null then 0 else eo_scrap end )   eando_scrap ,
   sum( case when po_received is null then 0 else po_received end ) po_rcv ,
   '' eo_usage

from eo_dashboard.stg_eo_usage 
where 
fiscal_year=:PER_YEAR
and fiscal_period=:PER_NUM
group by 
fiscal_year,
 fiscal_period,
 organization_code,
 organization_id,
 inventory_item_id,
 inventory_item