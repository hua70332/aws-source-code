
select ipr.*,
eo.period_year,
eo.period_num,
eo.period_name 
 from 
XXSC.XXSC_EO_IPR_ISO_V  ipr ,
 APPS.ORG_ACCT_PERIODS  eo
   where  trunc(ipr.ipr_date) between eo.period_start_date and eo.period_close_date
   and (ipr.source_organization_id = eo.organization_id or
        ipr.destination_organization_id = eo.organization_id )
   and eo.period_num=:PER_NUM
and eo.period_year=:PER_YEAR
   
 
