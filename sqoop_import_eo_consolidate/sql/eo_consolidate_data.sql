insert overwrite  table eo_dashboard.stg_export_eo_consolidate
select 
trans_onhand.fiscal_year,
trans_onhand.fiscal_quarter fiscal_qtr,
trans_onhand.fiscal_period ,
trans_onhand.period_name ,
trans_onhand.organization_code ,
trans_onhand.organization_id ,
trans_onhand.item,
items.user_item_type item_type ,
items.product_hierarchy prod_hierarchy ,
bus.l3_business_group_desc l3_bus_group,
bus.l4_business_unit_desc l4_bus_unit,
items.product_family gl_line_code  ,
plm.prode_line_desc gl_line_descr ,
trans_onhand.item_id,
items.description  item_descr,
trans_onhand.item_cost,
items.inventory_item_status_code item_status ,
items.planner planner_code,
items.planner_name planner_name,
items.default_buyer buyer_name ,
plm.x_plm_emp_name plm_name ,
trans_onhand.lite_org_code,
(case when trans_onhand.net_oh is null then 0 else  trans_onhand.net_oh  end + case when trans_onhand.non_net_oh is null then 0 else  trans_onhand.non_net_oh  end  + case when trans_onhand.open_po is null then 0 else  trans_onhand.open_po  end  + case when wo.total_open_wo is null then 0 else  wo.total_open_wo  end ) total_supply ,
trans_onhand.net_oh,
trans_onhand.srv_net_oh,
trans_onhand.loan_net_oh,
trans_onhand.non_net_oh,
trans_onhand.mrb_non_net_oh,
trans_onhand.rma_non_net_oh,
wo.total_open_wo  total_open_wo,
trans_onhand.open_po  open_po,
trans_onhand.po_rcv  po_rcv,  
 trans_onhand.std_ord_ship ,
 trans_onhand.con_ord_ship ,
 trans_onhand.wip_issue ,
 trans_onhand.iso_ship,
 trans_onhand.misc_issue,
trans_onhand.eando_scrap   ,
from_utc_timestamp(current_timestamp ,'America/Los_Angeles') creation_date    ,
from_utc_timestamp(current_timestamp ,'America/Los_Angeles') last_update_date ,
items.default_shipping_org org_descr       ,
items.make_or_buy item_procure   ,
bus.l6_product_family_desc l6_module      ,
trans_onhand.wip_completion           ,
trans_onhand.po_rcv_intransit         ,
trans_onhand.int_order_intransit      ,   
trans_onhand.int_order_rcv 
from 

(
select 
a.fiscal_year, a.fiscal_period,
 ea.quarter_num fiscal_quarter ,
 ea.period_name,
a.organization_code,
a.inventory_item item,
a.organization_id ,
a.inventory_item_id item_id,
a.organization_code lite_org_code ,  
SUM(NVL(std_ord_ship,0)) std_ord_ship,
SUM(NVL(con_ord_ship,0)) con_ord_ship,
SUM(NVL(wip_issue,0)) wip_issue,
SUM(NVL(wip_completion,0)) wip_completion,
SUM(NVL(iso_ship,0)) iso_ship,
SUM(NVL(int_order_rcv,0)) int_order_rcv,
SUM(NVL(misc_issue,0)) misc_issue,
SUM(NVL(eando_scrap,0)) eando_scrap,
SUM(NVL(po_rcv,0)) po_rcv,
SUM(NVL(net_oh,0)) net_oh,
SUM(NVL(srv_net_oh,0)) srv_net_oh,
SUM(NVL(loan_net_oh,0)) loan_net_oh,
SUM(NVL(non_net_oh,0)) non_net_oh,
SUM(NVL(mrb_non_net_oh,0)) mrb_non_net_oh,
SUM(NVL(rma_non_net_oh,0)) rma_non_net_oh,
SUM(NVL(int_order_intransit,0)) int_order_intransit,
SUM(NVL(po_rcv_intransit,0)) po_rcv_intransit,
SUM(NVL(open_po,0)) open_po  ,
SUM(NVL(item_cost,0)) item_cost,
SUM(NVL(total_onhand,0)) total_onhand

from
(
(
select
 et.fiscal_year,
-- ea.quarter_num fiscal_quarter ,
 et.fiscal_period,
-- ea.period_name,
 et.organization_code,
 et.organization_id,
 et.inventory_item_id,
 et.inventory_item  ,
 et.std_ord_ship,
 et.con_ord_ship,
 et.wip_issue,
 et.wip_completion,
 et.iso_ship,
 et.int_order_rcv,
 et.misc_issue ,
 et.eando_scrap,
 et.po_rcv,
 0  open_po,
 0  net_oh,
 0  srv_net_oh,
 0  loan_net_oh,
 0  non_net_oh,
 0  mrb_non_net_oh,
 0  rma_non_net_oh,
 0  int_order_intransit,
 0  po_rcv_intransit,
 0  item_cost,
 0  total_onhand

   from eo_dashboard.per_eo_usage et
--    ,eo_dashboard.tbl_eo_acct_periods ea
  where 1=1
-- and et.fiscal_year = ea.period_year 
-- and et.fiscal_period = ea.period_num
    and et.fiscal_year = :PER_YEAR
    and et.fiscal_period = :PER_NUM
 )
-- full outer join 
union all

( 
select
 oh.fiscal_year,
-- ea.quarter_num fiscal_quarter ,
 oh.fiscal_period,
-- ea.period_name,
 oh.organization_code,
 oh.organization_id,
 oh.inventory_item_id,
 oh.inventory_item  ,
 0  std_ord_ship,
 0  con_ord_ship,
 0  wip_issue,
 0  wip_completion,
 0  iso_ship,
 0  int_order_rcv,
 0  misc_issue ,
 0  eando_scrap,
 0  po_rcv,
 0  open_po,
 oh.eo_onhand net_oh,
  oh.srv_onhand srv_net_oh,
 oh.loan_onhand loan_net_oh,
 oh.non_net_onhand non_net_oh,
 oh.mrb_nonnettable mrb_non_net_oh,
 oh.rma_nonnettable rma_non_net_oh,
 oh.int_order_intransit,
 oh.po_rcv_intransit,
 oh.item_cost,
 oh.total_onhand
--select oh.*, ea.* 
from  eo_dashboard.stg_eo_onhand oh
-- , eo_dashboard.tbl_eo_acct_periods ea 
where 1=1
--and oh.organization_code not in ('391','251')  
--and ea.period_year=oh.fiscal_year
--and ea.period_num = oh.fiscal_period
and oh.fiscal_year = :PER_YEAR
and oh.fiscal_period = :PER_NUM
 ) 
 
union all 

( select
latest.period_year fiscal_year,
latest.period_num   fiscal_period,
po.org_num organization_code,
cast(substr(itm.item_org_identifier , instr(itm.item_org_identifier , '~' )+1) as int ) organization_id,
cast(substr(itm.item_org_identifier ,1, instr(itm.item_org_identifier , '~' )-1) as int ) inventory_item_id,
po.product_number inventory_item ,
 0  std_ord_ship,
 0  con_ord_ship,
 0  wip_issue,
 0  wip_completion,
 0  iso_ship,
 0  int_order_rcv,
 0  misc_issue ,
 0  eando_scrap,
 0  po_rcv,
 sum(po.shipment_quantity_open) open_po,
 0  net_oh,
 0  srv_net_oh,
 0  loan_net_oh,
 0  non_net_oh,
 0  mrb_non_net_oh,
 0  rma_non_net_oh,
 0  int_order_intransit,
 0  po_rcv_intransit,
 0  item_cost,
 0  total_onhand
from orclapps.tbl_po_commitment po
,data_analytics.tbl_eo_lpn_data_oracle  itm
,(SELECT line_location_id,MAX(loaded_date) AS last_load, e1.period_year, e1.period_num
  FROM orclapps.tbl_po_commitment p, eo_dashboard.tbl_eo_acct_periods e1
  where p.loaded_date between e1.start_date and date_add(e1.end_date,1) 
  and e1.period_num= :PER_NUM and e1.period_year= :PER_YEAR 
  GROUP BY line_location_id , e1.period_year, e1.period_num
)latest
where 1=1
 AND cast(latest.line_location_id as int) = cast(po.line_location_id as int)
AND CAST(latest.last_load AS date) = CAST(po.loaded_date AS date)
and po.product_number NOT LIKE 'Z-%' -- rule 1
AND po.product_number NOT LIKE '%OSP%' -- rule 1
AND UPPER(po.supplier_name) NOT like '%LUMENTUM%' -- rule 2
AND UPPER(po.supplier_name) NOT LIKE 'CELESTICA%' -- rule 2
AND UPPER(po.supplier_name) NOT LIKE 'FABRINET%' -- rule 2
AND UPPER(po.supplier_name) NOT LIKE 'HISENSE%' -- rule 2
AND UPPER(po.supplier_name) NOT LIKE 'NLIGHT%' -- rule 2
AND UPPER(po.supplier_name) NOT LIKE 'SANMINA%' -- rule 2
AND UPPER(po.supplier_name) NOT LIKE 'VENTURE%' -- rule 2
AND UPPER(po.supplier_name) NOT LIKE 'ZZZ%' -- rule 2
AND po.internal_external_supplier != 'Internal' -- rule 2
AND po.purchase_cycle_status IN ('OPEN', 'CLOSED FOR INVOICE') -- rule 4
AND po.purchase_approval_status = 'APPROVED' -- rule 5 (confirmed with Jerushah)
AND po.shipment_quantity_open > 0

AND ( 
-- rule 2. exclude closed PO's PAR-540 
CASE
  WHEN po.x_closed_code = 'CLOSED' THEN 'exclude'
  ELSE 'include'
END ) = 'include'
and po.org_num = itm.organization_code
and po.product_number = itm.item 
group by latest.period_year, latest.period_num, 
po.org_num ,po.product_number, itm.item_org_identifier  ) 
 ) a

,eo_dashboard.tbl_eo_acct_periods ea
WHERE a.fiscal_year = ea.period_year
and a.fiscal_period = period_num
GROUP BY a.fiscal_year, a.fiscal_period, a.organization_code, a.organization_id,a.inventory_item_id, a.inventory_item
,ea.quarter_num,ea.period_name
) trans_onhand
inner join data_analytics.tbl_eo_lpn_data_oracle items  on ( trans_onhand.item = items.item  and  trans_onhand.organization_code = items.organization_code ) 
  
  left outer join 

( 
select    
 wog.assembly_item,
 wog.job_organization_code ,
 wog.period_name, 
 sum(quantity_remaining)  total_open_wo
 from
(
select
 w.wip_entity_id,
 w.assembly_item,
w.job_organization_code ,
eo.period_name, 
w.quantity_remaining 
from orclapps.tbl_work_orders w ,eo_dashboard.tbl_eo_acct_periods eo 
 where 
 eo.period_year= :PER_YEAR and eo.period_num= :PER_NUM and
 
   cast(to_date(from_unixtime(unix_timestamp(w.scheduled_start_date, 'yyyy-MM-dd HH:mm:ss'))) as date)  >= eo.start_date and
   cast(to_date(from_unixtime(unix_timestamp(w.scheduled_start_date, 'yyyy-MM-dd HH:mm:ss'))) as date)  <= eo.end_date 
   group by 
  w.wip_entity_id,
  w.assembly_item,
  w.job_organization_id,
  w.job_organization_code ,
  eo.period_name ,
  w.quantity_remaining) wog
 group by 
 wog.assembly_item,
 wog.job_organization_code ,
 wog.period_name) wo 
 
      on 
     ( wo.assembly_item=trans_onhand.item 
       and  wo.job_organization_code=trans_onhand.organization_code
       and   wo.period_name=trans_onhand.period_name )

left join 

( SELECT prod_line_code,prode_line_desc ,x_plm_emp_name FROM rma.tbl_product_hierarchy_item  WHERE x_plm_emp_name != 'null' group by prod_line_code,prode_line_desc,x_plm_emp_name ) plm

on (items.product_family = plm.prod_line_code ) 

left join 

(select * from orclapps.tbl_sales_hierarchy) bus on (items.product_family = bus.gl_product_line )  


where 1=1
and trans_onhand.fiscal_year = :PER_YEAR
and trans_onhand.fiscal_period = :PER_NUM
and lower(items.user_item_type) not in ( 'expensequantitytracked' , 'expense item' , 'reference item' , 'service item' , 'capital item' )   -- Exclude expense item
and trans_onhand.organization_code not in ('201','391')                     -- Exclude unused org
--and ( (NVL(trans_onhand.open,0)= 0 and  NVL(trans_onhand.item_cost,0) = 0 ) 
--or (NVL(trans_onhand.open_po,0) <> 0 ) )
AND (NVL(trans_onhand.open_po,0) > 0  or                                  -- Exclude items without cost, open po, onhand and transaction
    ( nvl(trans_onhand.net_oh, 0) + abs(nvl(trans_onhand.po_rcv,0)) + nvl(trans_onhand.po_rcv_intransit,0) 
        + abs(nvl(trans_onhand.std_ord_ship,0)) + abs(nvl(trans_onhand.con_ord_ship,0)) + abs(nvl(trans_onhand.wip_issue,0))
        + abs(nvl(trans_onhand.wip_completion,0)) + abs(nvl(trans_onhand.iso_ship,0)) + abs(nvl(trans_onhand.int_order_rcv,0))
        + abs(nvl(trans_onhand.int_order_intransit,0)) + abs(nvl(trans_onhand.misc_issue,0)) + abs(nvl(trans_onhand.eando_scrap ,0)) 
+ abs(nvl(trans_onhand.srv_net_oh,0)) + abs(nvl(trans_onhand.loan_net_oh,0)) <>0 
       and NVL(trans_onhand.item_cost,0) > 0))