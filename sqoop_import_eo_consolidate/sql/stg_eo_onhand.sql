SELECT 
            FISCAL_YEAR
            ,FISCAL_PERIOD
            ,INVENTORY_ITEM_ID
            ,INVENTORY_ITEM
            ,ORGANIZATION_ID
            ,ORGANIZATION_CODE
            ,PRIMARY_UOM
--            ,pt.SUBINVENTORY_CODE
--            ,RTRIM(param.ORGANIZATION_CODE)||'.'||RTRIM(item.SEGMENT1) MAPPING_CODE
            ,SUM(TOTAL_ONHAND) TOTAL_ONHAND
            ,SUM(NON_NET_ONHAND) NON_NET_ONHAND
            ,SUM(EO_ONHAND) EO_ONHAND
            ,SUM(MRB_NONNETTABLE) MRB_NONNETTABLE
            ,SUM(RMA_NONNETTABLE) RMA_NONNETTABLE
            ,SUM(SRV_ONHAND) SRV_ONHAND
            ,SUM(LOAN_ONHAND) LOAN_ONHAND
            ,SUM(INT_ORDER_INTRANSIT) INT_ORDER_INTRANSIT
            ,SUM(PO_RCV_INTRANSIT) PO_RCV_INTRANSIT
            ,(
                SELECT ROUND(A.STANDARD_COST,5)
                FROM (SELECT inventory_item_id,organization_id,STANDARD_COST,STANDARD_COST_REVISION_DATE
                      FROM apps.CST_STANDARD_COSTS
                      ORDER BY inventory_item_id,organization_id,STANDARD_COST_REVISION_DATE DESC) A
                WHERE 1=1
                AND A.inventory_item_id = txn.INVENTORY_ITEM_ID
                AND A.organization_id   = txn.ORGANIZATION_ID 
                AND TRUNC(A.STANDARD_COST_REVISION_DATE) <= txn.P_END_DATE
                AND ROWNUM = 1
               ) ITEM_COST
FROM (
    SELECT 
            gp.PERIOD_YEAR AS FISCAL_YEAR
            ,gp.PERIOD_NUM AS FISCAL_PERIOD
            ,gp.END_DATE AS P_END_DATE
            ,pt.INVENTORY_ITEM_ID
            ,item.SEGMENT1 INVENTORY_ITEM
            ,pt.ORGANIZATION_ID
            ,param.ORGANIZATION_CODE
            ,item.PRIMARY_UOM_CODE PRIMARY_UOM
--            ,pt.SUBINVENTORY_CODE
            ,SUM(pt.PRIMARY_QUANTITY) TOTAL_ONHAND
            ,SUM(pt.NON_NET_ONHAND) NON_NET_ONHAND
            ,SUM(pt.EO_ONHAND) + SUM(pt.INT_ORDER_INTRANSIT) + SUM(pt.PO_RCV_INTRANSIT) EO_ONHAND
            ,SUM(pt.MRB_NONNETTABLE) MRB_NONNETTABLE
            ,SUM(pt.RMA_NONNETTABLE) RMA_NONNETTABLE
            ,SUM(pt.SRV_ONHAND) SRV_ONHAND
            ,SUM(pt.LOAN_ONHAND) LOAN_ONHAND
            ,SUM(pt.INT_ORDER_INTRANSIT) INT_ORDER_INTRANSIT
            ,SUM(pt.PO_RCV_INTRANSIT) PO_RCV_INTRANSIT
       FROM (
/*********************************************************************************************
* Total up inventory transaction quantity of Period for calculating on-hand of period        *
* Calculation Rules:                                                                         *
* 1. Exclude transaction which subinventory is not EO inventory                              *
* 2. Sum transaction quantity by subinventory type and fiscal month                          *
*********************************************************************************************/ 
             SELECT 
                 mmt.INVENTORY_ITEM_ID
                 ,mmt.ORGANIZATION_ID
--                 ,mmt.SUBINVENTORY_CODE
                 ,SUM(mmt.PRIMARY_QUANTITY) AS PRIMARY_QUANTITY
                 ,SUM(CASE                              -- Onhand with E and O rule
                    WHEN --MSIC.AVAILABILITY_TYPE = 1 
                        msic.ASSET_INVENTORY = 1
						AND NOT EXISTS(SELECT 'X' FROM GL.GL_CODE_COMBINATIONS gcci 
                                 WHERE (gcci.SEGMENT3 IN ('14313','14217') OR gcci.SEGMENT3 LIKE '6%') 
                                        AND msic.MATERIAL_ACCOUNT = gcci.CODE_COMBINATION_ID(+)) 
                         AND NOT EXISTS(SELECT 'X' FROM dual 
                                        WHERE mmt.SUBINVENTORY_CODE IN ('ADVRPL-ESI', 'ESI SG RMA', 'LSR SRVFBN') 
                                        OR mmt.SUBINVENTORY_CODE LIKE 'x%')
                    THEN
                        mmt.PRIMARY_QUANTITY
                    ELSE
                        0
                    END) EO_ONHAND
                 ,SUM(CASE                              -- Non Nettable onhand
                        WHEN     MSIC.AVAILABILITY_TYPE = 2
                        THEN
                           mmt.PRIMARY_QUANTITY
                        ELSE
                           0
                     END) NON_NET_ONHAND
                 ,SUM(CASE                              -- MRB Nonnettable Onhand
                    WHEN EXISTS(SELECT 'X' FROM dual WHERE MSIC.AVAILABILITY_TYPE = 2 
                        AND (mmt.SUBINVENTORY_CODE LIKE '%MRB%' 
                            OR mmt.SUBINVENTORY_CODE LIKE '%REWORK%' 
                            OR mmt.SUBINVENTORY_CODE LIKE '%RTV%'))
                    THEN
                        mmt.PRIMARY_QUANTITY
                    ELSE
                        0
                    END) MRB_NONNETTABLE
                 ,SUM(CASE                              -- RMA Nonnettable Onhand
                    WHEN MSIC.AVAILABILITY_TYPE = 2 AND mmt.SUBINVENTORY_CODE LIKE '%RMA%' 
                    THEN
                        mmt.PRIMARY_QUANTITY
                    ELSE
                        0
                    END) RMA_NONNETTABLE
                 ,SUM(CASE                              -- Service Onhand
                    WHEN mmt.SUBINVENTORY_CODE LIKE '%SRV%' OR mmt.SUBINVENTORY_CODE LIKE '%RSC%'
                    THEN
                        mmt.PRIMARY_QUANTITY
                    ELSE
                        0
                    END) SRV_ONHAND
                 ,SUM(CASE                               -- Loan Onhand
                    WHEN mmt.SUBINVENTORY_CODE LIKE '%LOAN%' 
                    THEN
                        mmt.PRIMARY_QUANTITY
                    ELSE
                        0
                    END) LOAN_ONHAND
                 ,SUM(INT_ORDER_SHIP + INT_ORDER_RCV) INT_ORDER_INTRANSIT
                 ,SUM(mmt.PO_RCV) AS PO_RCV_INTRANSIT
				 ,mmt.PERIOD_START_DATE
             FROM 
				(
				SELECT mmt.INVENTORY_ITEM_ID
                    ,mmt.TRANSFER_ORGANIZATION_ID AS ORGANIZATION_ID
                    ,'IN_TRANSIT' AS SUBINVENTORY_CODE
					,0 AS PRIMARY_QUANTITY
					,mmt.PRIMARY_QUANTITY AS INT_ORDER_SHIP
					,0 AS INT_ORDER_RCV
					,0 AS PO_RCV
					,oap.PERIOD_START_DATE
				FROM APPS.MTL_MATERIAL_TRANSACTIONS mmt 
				     ,APPS.ORG_ACCT_PERIODS oap 
				WHERE 1=1
				AND mmt.ACCT_PERIOD_ID = oap.ACCT_PERIOD_ID
				AND mmt.ORGANIZATION_ID = oap.ORGANIZATION_ID
--				AND EXISTS(SELECT 1 FROM dual WHERE MONTHS_BETWEEN(SYSDATE,oap.PERIOD_START_DATE) <= 12)
				AND mmt.TRANSACTION_TYPE_ID = 62
--				AND mmt.INVENTORY_ITEM_ID = 84235
				UNION ALL
				SELECT mmt.INVENTORY_ITEM_ID
                    ,mmt.ORGANIZATION_ID
                    ,mmt.SUBINVENTORY_CODE
					,mmt.PRIMARY_QUANTITY * -1 AS PRIMARY_QUANTITY
					,0 AS INT_ORDER_SHIP
                    ,(CASE WHEN mmt.TRANSACTION_TYPE_ID = 61
                           THEN
                                mmt.PRIMARY_QUANTITY
                            ELSE
                                0
                      END) AS INT_ORDER_RCV
                    ,(CASE WHEN mmt.TRANSACTION_TYPE_ID IN (18, 36, 71)
                           THEN
                                mmt.PRIMARY_QUANTITY
                            ELSE
                                0
                      END) AS PO_RCV
					,oap.PERIOD_START_DATE
				FROM APPS.MTL_MATERIAL_TRANSACTIONS mmt 
				     ,APPS.ORG_ACCT_PERIODS oap 
				WHERE 1=1
				AND mmt.ACCT_PERIOD_ID = oap.ACCT_PERIOD_ID
				AND mmt.ORGANIZATION_ID = oap.ORGANIZATION_ID
--				AND EXISTS(SELECT 1 FROM dual WHERE MONTHS_BETWEEN(SYSDATE,oap.PERIOD_START_DATE) <= 12)
				AND mmt.PRIMARY_QUANTITY <> 0
                AND (mmt.LOGICAL_TRANSACTION IS NULL OR (NOT mmt.LOGICAL_TRANSACTION IS NULL AND mmt.TRANSACTION_ID = mmt.PARENT_TRANSACTION_ID))
                AND NOT EXISTS(SELECT 'X' FROM dual WHERE mmt.TRANSACTION_TYPE_ID = '10008')
--                AND mmt.INVENTORY_ITEM_ID = 96539
				  ) mmt			            
                 ,INV.MTL_SECONDARY_INVENTORIES msic                 
            WHERE 1 = 1
                  AND msic.ORGANIZATION_ID(+) = mmt.ORGANIZATION_ID
                  AND msic.SECONDARY_INVENTORY_NAME(+) = mmt.SUBINVENTORY_CODE
            GROUP BY mmt.INVENTORY_ITEM_ID,mmt.ORGANIZATION_ID,mmt.PERIOD_START_DATE --, mmt.SUBINVENTORY_CODE
UNION ALL 
/*********************************************************************************************
* Sum latest on-hand stock for calculating on-hand of period                                 *
* Calculation Rules:                                                                         *
* 1. Exclude on-hand stock which subinventory is not EO inventory                            *
* 2. Sum on-hand stock by subinventory type                                                  *
*********************************************************************************************/ 
             SELECT moq.INVENTORY_ITEM_ID
                   ,moq.ORGANIZATION_ID
--                   ,moq.SUBINVENTORY_CODE
                   ,SUM(moq.PRIMARY_TRANSACTION_QUANTITY) AS PRIMARY_QUANTITY
                   ,SUM((CASE                            -- EandO Onhand
                        WHEN     --MSIC.AVAILABILITY_TYPE = 1
                             msic.ASSET_INVENTORY = 1
                             AND NOT EXISTS
                                    (SELECT 'X'
                                       FROM GL.GL_CODE_COMBINATIONS gcci
                                      WHERE     (   gcci.SEGMENT3 IN
                                                       ('14313', '14217')
                                                 OR gcci.SEGMENT3 LIKE '6%')
                                            AND msic.MATERIAL_ACCOUNT =
                                                   gcci.CODE_COMBINATION_ID(+))
                             AND NOT EXISTS
                                    (SELECT 'X'
                                       FROM DUAL
                                      WHERE    moq.SUBINVENTORY_CODE IN
                                                  ('ADVRPL-ESI',
                                                   'ESI SG RMA',
                                                   'LSR SRVFBN')
                                            OR moq.SUBINVENTORY_CODE LIKE 'x%')
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       EO_ONHAND
                   ,SUM((CASE                            -- Non Nettable onhand
                        WHEN     MSIC.AVAILABILITY_TYPE = 2
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       NON_NET_ONHAND
                   ,SUM((CASE                           -- MRB Nonnettable Onhand
                        WHEN EXISTS
                                (SELECT 'X'
                                   FROM DUAL
                                  WHERE     MSIC.AVAILABILITY_TYPE = 2
                                        AND (   moq.SUBINVENTORY_CODE LIKE
                                                   '%MRB%'
                                             OR moq.SUBINVENTORY_CODE LIKE
                                                   '%REWORK%'
                                             OR moq.SUBINVENTORY_CODE LIKE
                                                   '%RTV%'))
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       MRB_NONNETTABLE
                   ,SUM((CASE                           -- RMA Nonnettable Onhand
                        WHEN     MSIC.AVAILABILITY_TYPE = 2
                             AND moq.SUBINVENTORY_CODE LIKE '%RMA%'
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       RMA_NONNETTABLE
                   ,SUM((CASE                           -- Service Onhand
                        WHEN moq.SUBINVENTORY_CODE LIKE '%SRV%' OR moq.SUBINVENTORY_CODE LIKE '%RSC%'
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       SRV_ONHAND
                   ,SUM((CASE                             -- Loan Onhand
                        WHEN moq.SUBINVENTORY_CODE LIKE '%LOAN%'
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       LOAN_ONHAND
                   ,0 AS INT_ORDER_INTRANSIT
                   ,0 AS PO_RCV_INTRANSIT
                   ,TO_DATE ('9999-12-31', 'YYYY-MM-DD') AS PERIOD_START_DATE
               FROM inv.MTL_ONHAND_QUANTITIES_DETAIL moq,
                    INV.MTL_SECONDARY_INVENTORIES msic
              WHERE     1 = 1
                    AND msic.ORGANIZATION_ID = moq.ORGANIZATION_ID
                    AND msic.SECONDARY_INVENTORY_NAME = moq.SUBINVENTORY_CODE
--                    AND moq.INVENTORY_ITEM_ID = 96539
              GROUP BY moq.INVENTORY_ITEM_ID,moq.ORGANIZATION_ID --,moq.SUBINVENTORY_CODE
 
    UNION ALL
/**********************************************************************************************
* Add record with 0 stock for below scenario                                                  *
* - There is inventory transactions but onhand is 0.                                          * 
*   (There is no record existing in the mtl_onhand_quantities_detail table)                   *
**********************************************************************************************/     
            SELECT DISTINCT mmt.INVENTORY_ITEM_ID
                  ,mmt.ORGANIZATION_ID
                  ,0 AS PRIMARY_QUANTITY
                ,0 AS EO_ONHAND
                ,0 AS NON_NET_ONHAND
                ,0 AS MRB_NONNETTABLE
                ,0 AS RMA_NONNETTABLE
                ,0 AS SRV_ONHAND
                ,0 AS LOAN_ONHAND
                ,0 AS INT_ORDER_INTRANSIT
                ,0 AS PO_RCV_INTRANSIT
                ,oap.SCHEDULE_CLOSE_DATE + 5 AS PERIOD_START_DATE
            FROM APPS.MTL_MATERIAL_TRANSACTIONS mmt 
                 ,APPS.ORG_ACCT_PERIODS oap 
            WHERE 1=1
            AND mmt.ACCT_PERIOD_ID = oap.ACCT_PERIOD_ID
            AND mmt.ORGANIZATION_ID = oap.ORGANIZATION_ID
--            AND oap.PERIOD_YEAR = 2022
--            AND mmt.INVENTORY_ITEM_ID = 1959873
            AND mmt.PRIMARY_QUANTITY <> 0
            AND (mmt.LOGICAL_TRANSACTION IS NULL OR (NOT mmt.LOGICAL_TRANSACTION IS NULL AND mmt.TRANSACTION_ID = mmt.PARENT_TRANSACTION_ID))
            AND NOT EXISTS(SELECT 'X' FROM dual WHERE mmt.TRANSACTION_TYPE_ID = '10008') 
    UNION ALL
/**********************************************************************************************
* Sum latest SUPPLY quantity of Period for calculating receive intransit of PO and Int Order  *
**********************************************************************************************/    
            SELECT ms.ITEM_ID INVENTORY_ITEM_ID
                ,ms.TO_ORGANIZATION_ID AS ORGANIZATION_ID
                ,0 AS PRIMARY_QUANTITY
                ,0 AS EO_ONHAND
                ,0 AS NON_NET_ONHAND
                ,0 AS MRB_NONNETTABLE
                ,0 AS RMA_NONNETTABLE
                ,0 AS SRV_ONHAND
                ,0 AS LOAN_ONHAND
                ,DECODE(NVL(PO_HEADER_ID,0),0,ROUND(ms.TO_ORG_PRIMARY_QUANTITY,6),0) AS INT_ORDER_INTRANSIT
                ,DECODE(NVL(ms.FROM_ORGANIZATION_ID,0),0,ROUND(ms.TO_ORG_PRIMARY_QUANTITY,6),0) AS PO_RCV_INTRANSIT
                ,TO_DATE ('9999-12-31', 'YYYY-MM-DD') AS PERIOD_START_DATE
            FROM inv.MTL_SUPPLY ms
            WHERE 1=1
            AND ms.SUPPLY_TYPE_CODE IN ( 'SHIPMENT' , 'RECEIVING') 
            AND ms.DESTINATION_TYPE_CODE = 'INVENTORY'
--            AND ms.ITEM_ID = 96539

    UNION ALL
/********************************************************************************************
* Total receiving transaction quantity of Period for calculating PO receive intransit       *
* Calculation Rules:                                                                        *
* 1. Sum reveived quantity by fiscal month                                                  *
* 2. Reduce quantity of Correct and Return to Vendor transaction posted at RECEIVINVG       *
*********************************************************************************************/     
        SELECT rsl.ITEM_ID INVENTORY_ITEM_ID
            ,rt.ORGANIZATION_ID
            ,0 AS PRIMARY_QUANTITY
            ,0 AS EO_ONHAND
            ,0 AS NON_NET_ONHAND
            ,0 AS MRB_NONNETTABLE
            ,0 AS RMA_NONNETTABLE
            ,0 AS SRV_ONHAND
            ,0 AS LOAN_ONHAND
            ,0 AS INT_ORDER_INTRANSIT
            ,(CASE 
                WHEN rt.TRANSACTION_TYPE = 'RECEIVE'
                THEN
                    ROUND(rt.PRIMARY_QUANTITY,6) * -1
                WHEN rt.TRANSACTION_TYPE = 'CORRECT'
                 AND EXISTS(SELECT 'X' FROM APPS.RCV_TRANSACTIONS rtr
                                WHERE rtr.TRANSACTION_TYPE = 'RECEIVE' 
                                AND rt.PARENT_TRANSACTION_ID = rtr.TRANSACTION_ID)
                THEN
                    ROUND(rt.PRIMARY_QUANTITY,6) * -1
                WHEN rt.TRANSACTION_TYPE = 'RETURN TO VENDOR'
                    AND rt.DESTINATION_TYPE_CODE = 'RECEIVING'
                THEN
                    ROUND(rt.PRIMARY_QUANTITY,6)
                ELSE
                    0
             END) AS PO_RCV_INTRANSIT
            ,gp.START_DATE AS PERIOD_START_DATE
        FROM APPS.RCV_TRANSACTIONS RT
           ,APPS.RCV_SHIPMENT_LINES RSL
           ,APPS.GL_PERIODS_V gp
        WHERE 1=1
        AND RSL.SHIPMENT_LINE_ID = RT.SHIPMENT_LINE_ID
        AND gp.PERIOD_SET_NAME = 'XXSC ACC CAL' 
--        AND rsl.SHIPMENT_LINE_STATUS_CODE = 'FULLY RECEIVED'
        AND EXISTS(SELECT 'X' FROM apps.PO_DISTRIBUTIONS_ALL pda 
                    WHERE pda.DESTINATION_TYPE_CODE = 'INVENTORY'
                        AND pda.PO_HEADER_ID = rsl.PO_HEADER_ID
                        AND pda.PO_LINE_ID = rsl.PO_LINE_ID
                        AND pda.LINE_LOCATION_ID = rsl.PO_LINE_LOCATION_ID)
        AND TRUNC(rt.TRANSACTION_DATE) BETWEEN gp.START_DATE AND gp.END_DATE
--        AND rt.TRANSACTION_TYPE = 'RECEIVE'
        AND rt.SOURCE_DOCUMENT_CODE = 'PO'
--        AND EXISTS(SELECT 1 FROM dual WHERE MONTHS_BETWEEN(SYSDATE,gp.START_DATE) BETWEEN 0 AND 12)
--        AND rsl.ITEM_ID = 1432649
        )  pt
		,APPS.GL_PERIODS_V gp
		,APPS.MTL_SYSTEM_ITEMS_B item
        ,INV.MTL_PARAMETERS param
      WHERE     1 = 1
			AND item.INVENTORY_ITEM_ID = pt.INVENTORY_ITEM_ID
			AND item.ORGANIZATION_ID = pt.ORGANIZATION_ID
			AND pt.ORGANIZATION_ID = param.ORGANIZATION_ID
			AND NOT param.ORGANIZATION_CODE IN ('201','391')
			AND gp.PERIOD_SET_NAME = 'XXSC ACC CAL'
                        AND gp.PERIOD_YEAR= :PER_YEAR
                        AND gp.PERIOD_NUM = :PER_NUM
			AND pt.PERIOD_START_DATE > gp.END_DATE
--			AND EXISTS(SELECT 1 FROM dual WHERE MONTHS_BETWEEN(SYSDATE,gp.START_DATE) BETWEEN 0 AND 12)

   GROUP BY pt.INVENTORY_ITEM_ID,
            item.SEGMENT1,
            pt.ORGANIZATION_ID,
            param.ORGANIZATION_CODE,
            item.PRIMARY_UOM_CODE,
--            pt.SUBINVENTORY_CODE,
            gp.PERIOD_YEAR,
            gp.PERIOD_NUM,
            gp.END_DATE
) txn
GROUP BY txn.FISCAL_YEAR, txn.FISCAL_PERIOD
            ,txn.INVENTORY_ITEM_ID
            ,txn.INVENTORY_ITEM
            ,txn.ORGANIZATION_ID
            ,txn.ORGANIZATION_CODE
            ,txn.PRIMARY_UOM
            ,txn.P_END_DATE